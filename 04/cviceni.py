a = [22, 3, 7774, 5, -8]


def minimum(a):
    minint = a[0]
    for i in a:
        if i < minint:
            minint = i
    return minint


def min_2(a):
    minint = a[0]
    for i in a:
        if i < minint:
            minint = i
    minint_2 = a[0]
    for i in a:
        if i < minint_2 and i != minint:
            minint_2 = i
    return minint_2


def printPoly(p):
    print(p[0], end=" ")
    for i, x in enumerate(p[1:]):
        print("+" if x > 0 else "-", str(x) + "x^" + str(i + 1), end=" ")


def value(p, x): return sum([p[i] * x ** i for i in range(len(p))])


def minVal(p, a, b):
    minint = value(p, a)
    x = a
    while x < b:
        x += 0.1
        if value(p,x) < minint:
            minint = value(p,x)
    return minint


p = [10, -1]
print(minVal(p, 1, 3))
