import sys
from enum import Enum

"""

Napište program compose.py, který načte řetězec cifer na první řádce a celé číslo N na řádce druhé. Program vloží znaky
 +, * mezi zadané cifry tak, aby hodnota vytvořeného výrazu byla N
Vstup:
    první řádka řetězec cifer 0-9
    druhá řádka celé kladné číslo
Testované úlohy mohou mít více řešení, v tom případě vytiskněte jedno libovolné řešení, nebo úloha nemá žádné 
řešení a pak vytiskněte: NO_SOLUTION
POZOR: Program může být výpočetně náročnější, otestujte si nejdříve Váš program na počítači a pouze důkladně otestovaný 
program nahrávejte do odevzdávacího systému
Nezapomeňte, že násobení má větší prioritu než sčítání
Celé dva body získají jen programy, které pracují chytřeji než zkoušení všech možností
    Jedna možnost zrychlit program je, pokud při vyhodnocování překročí hodnota sčítanců cílovou hodnotu, tak již není 
    potřeba testovat zbytek výrazu a umisťovat tam znaménka výpočtu.

"""
HW_4H_OPERATORS = ("+", "*")


def print_and_exit(string):
    print(string)
    sys.exit()


class Action(Enum):
    Add = 1
    Multiply = 2
    Keep = 4


def rpn_eval(notated_list, startingval=0):
    stack = []
    if startingval: stack = [startingval]
    print(notated_list)
    for token in notated_list:
        match token:
            case "+":
                y, x = stack.pop(), stack.pop()
                z = x + y
            case "*":
                y, x = stack.pop(), stack.pop()
                z = x * y
            case _:
                z = token
        stack.append(z)
    return stack.pop()


def iteration(result: str, action: Action.Add, to_process: tuple, stack, wanted_result, int_buffer, current_tally):
    new_to_process = to_process[1:]
    new_stack = stack.copy()

    match action:
        case Action.Add:
            new_stack.append(int_buffer)
            new_stack.append("+")  # Adds [buffer, "+"] to the stack
            current_tally = rpn_eval(new_stack.copy())  # executes the add
            if current_tally > wanted_result:  # overshoot
                return 0
            int_buffer = to_process[0]  # resets the bufffer
            result += "+" + str(int_buffer)
        case Action.Multiply:
            new_stack.append(int_buffer)
            new_stack.append("*")  # Adds [buffer, "+"] to the stack
            new_to_process = to_process[1:]  # removes the newly added int to
            current_tally = rpn_eval(new_stack.copy(), current_tally)  # executes the add
            if current_tally > wanted_result:  # overshoot
                return 0
            int_buffer = to_process[0]  # resets the bufffer
            result += "*" + str(int_buffer)
        case Action.Keep:
            int_buffer = 10 * int_buffer + to_process[0]  # add a number to the buffer
            result += str(to_process[0])  # we are not doing anything to the num
        case _:
            raise Exception("Nonsense action")

    if not (len(new_to_process)):
        if current_tally == wanted_result:
            print_and_exit(result)
        else:
            return 0
    print(current_tally)
    print(stack)
    print("----")
    iteration(result, Action.Add, new_to_process, new_stack, wanted_result, int_buffer, current_tally)
    iteration(result, Action.Multiply, new_to_process, new_stack, wanted_result, int_buffer, current_tally)
    iteration(result, Action.Keep, new_to_process, new_stack, wanted_result, int_buffer, current_tally)


def main():
    # to_process = tuple([int(x) for x in input()])
    # wanted_result = int(input())

    to_process = tuple([int(x) for x in "98706543"])
    wanted_result = int("34")


    iteration(str(to_process[0]), Action.Add, to_process[1:], [to_process[0], ], wanted_result, 0, 0)
    iteration(str(to_process[0]), Action.Multiply, to_process[1:], [to_process[0], ], wanted_result, 0, 0)
    iteration(str(to_process[0]), Action.Keep, to_process[1:], [to_process[0], ], wanted_result, 0, 0)

    return


# iteration(Action.Add)
# iteration(Action.Multiply)
# iteration(Action.Keep)

if __name__ == "__main__":
    print(rpn_eval([9, 0, '+', 8, '+', 7, '+', 0, '*', 6, '+', 5, '*', 4, '+']))
