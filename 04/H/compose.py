import sys
from enum import Enum

"""

Napište program compose.py, který načte řetězec cifer na první řádce a celé číslo N na řádce druhé. Program vloží znaky
 +, * mezi zadané cifry tak, aby hodnota vytvořeného výrazu byla N
Vstup:
    první řádka řetězec cifer 0-9
    druhá řádka celé kladné číslo
Testované úlohy mohou mít více řešení, v tom případě vytiskněte jedno libovolné řešení, nebo úloha nemá žádné 
řešení a pak vytiskněte: NO_SOLUTION
POZOR: Program může být výpočetně náročnější, otestujte si nejdříve Váš program na počítači a pouze důkladně otestovaný 
program nahrávejte do odevzdávacího systému
Nezapomeňte, že násobení má větší prioritu než sčítání
Celé dva body získají jen programy, které pracují chytřeji než zkoušení všech možností
    Jedna možnost zrychlit program je, pokud při vyhodnocování překročí hodnota sčítanců cílovou hodnotu, tak již není 
    potřeba testovat zbytek výrazu a umisťovat tam znaménka výpočtu.

"""
HW_4H_OPERATORS = ("+", "*")


def print_and_exit(string):
    print(string)
    sys.exit()




# def rpn_eval(notated_list, startingval=0):
#     stack = []
#     if startingval: stack = [startingval]
#     # print(notated_list)
#     for token in notated_list:
#         match token:
#             case "+":
#                 y, x = stack.pop(), stack.pop()
#                 z = x + y
#             case "*":
#                 y, x = stack.pop(), stack.pop()
#                 z = x * y
#             case _:
#                 z = token
#         stack.append(z)
#     return stack.pop()

class Action(Enum):
    Add = 1
    Multiply = 2
    Keep = 4

def iteration(result: str, action: Action.Add, to_process: tuple, wanted_result, int_buffer):
    new_to_process = to_process[1:]

    if action == Action.Add:
        int_buffer = to_process[0]  # resets the bufffer
        result += " + " + str(int_buffer)
    elif action == Action.Multiply:
        new_to_process = to_process[1:]  # removes the newly added int to
        int_buffer = to_process[0]  # resets the bufffer
        result += " * " + str(int_buffer)
    elif action == Action.Keep:
        int_buffer = 10 * int_buffer + to_process[0]  # add a number to the buffer
        result += str(to_process[0])  # we are not doing anything to the num
    else:
        raise Exception("Nonsense action")

    # print(result)
    current_tally = eval(" ".join(
        [s.lstrip("0") if s != "0" else "0" for s in result.split(" ")]
        )
    )

    if current_tally > wanted_result:
        # print(result)
        return 0

    if not (len(new_to_process)):
        # print(result)
        if current_tally == wanted_result:
            print_and_exit(result.replace(" ",""))
        else:
            return 0
    # print(current_tally)
    # print(stack)
    # print("----")
    iteration(result, Action.Add, new_to_process, wanted_result, int_buffer)
    iteration(result, Action.Multiply, new_to_process, wanted_result, int_buffer)
    iteration(result, Action.Keep, new_to_process, wanted_result, int_buffer)


def main():
    # to_process = tuple([int(x) for x in input()])
    # wanted_result = int(input())

    to_process = tuple([int(x) for x in "354227808"])
    # # print(to_process)
    wanted_result = int("25")



    iteration(str(to_process[0]), Action.Add, to_process[1:], wanted_result, 0)
    iteration(str(to_process[0]), Action.Multiply, to_process[1:], wanted_result, 0)
    iteration(str(to_process[0]), Action.Keep, to_process[1:], wanted_result, 0)

    # fault - no solutions found
    print_and_exit("NO_SOLUTION")

    return


# iteration(Action.Add)
# iteration(Action.Multiply)
# iteration(Action.Keep)

if __name__ == "__main__":
    main()
