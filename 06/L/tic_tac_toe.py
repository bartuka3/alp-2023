#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import sys

NEIGHBOURS = (
    (0, 1),
    (1, 0),
    (-1, 0),
    (0, -1)
)


def vn(v, n: int):
    """Multiplies a 2D vector with a constant"""
    return [v[0] * n, v[1] * n]


def v_w(v, w):
    """Adds two 2D vectors"""
    return [v[0] + w[0], v[1] + w[1]]


def is_in_constraint(y, x, *args):
    for arg in args:
        # print(y,x,arg)
        if arg[0] in range(0, y) and arg[1] in range(0, x):
            return True
        else:
            return False


def count_marks(board):
    x = 0
    o = 0
    for line in board:
        for char in line:
            if char == "x":
                x += 1
            elif char == "o":
                o += 1
    return x == o and x and o


def get_char(flag: bool):
    return "o" if flag else "x"


def find_lines(board, flag):
    fill_board = [[0 for _ in range(len(board[0]))] for _ in range(len(board))]
    lines = []
    line_stack = []
    wanted_char = get_char(flag)
    for y in range(len(board)):
        for x in range(len(board[0])):
            if not fill_board[y][x] and board[y][x] == wanted_char:
                fill_board[y][x] = 1
                for neigh in NEIGHBOURS:
                    n = 1
                    nv = v_w([y, x], vn(neigh, n))
                    print(y,x,nv,neigh)
                    while is_in_constraint(len(board), len(board[0]), nv) and board[nv[0]][nv[1]] == wanted_char:
                        # fill_board[nv[0]][nv[1]] = 1
                        n += 1
                        nv = v_w([y, x], vn(neigh, n))
                    print(n)
                    if n == 4:  # 4 chars in a row
                        lines.append([[y, x], v_w([y, x], vn(neigh, n - 1)), neigh])
    return lines


def load_board(fn):
    board = []
    with open(fn, "r") as file:
        for line in file:
            board.append(line.split())
    return board


def check_spaces(board, line):
    nv = v_w(line[0], vn(line[2], -1))
    if is_in_constraint(len(board), len(board[0]), nv) and board[nv[0]][nv[1]] == ".":
        return " ".join(map(str, nv))
    nnv = v_w(line[1], vn(line[2], 1))
    if is_in_constraint(len(board), len(board[0]), nnv) and board[nnv[0]][nnv[1]] == ".":
        return " ".join(map(str, nv))


def main():
    # fn = sys.argv[1]
    fn = "6254.txt"
    board = load_board(fn)

    lines = find_lines(board, count_marks(board))
    # print(lines)
    print(check_spaces(board, lines[0]))


if __name__ == "__main__":
    main()
