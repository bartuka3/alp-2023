import copy
from os import system
import time
import random
a = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
     [0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
     [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
   ]


vecs = ((1, 1), (0, 1), (1, 0), (1, -1))


def life(iter,a):
    b = copy.deepcopy(a)
    for i in range(len(a)):
        for j in range(len(a[0])):  # Pro každou buňku
            cell_counter = 0
            for position in vecs:
                if a[(i + position[0]) % len(a)][(j + position[1]) % len(a[0])] == 1:
                    cell_counter += 1
                if a[(i - position[0]) % len(a)][(j - position[1]) % len(a[0])] == 1:
                    cell_counter += 1
            if cell_counter == 3:
                b[i][j] = 2
            elif cell_counter != 2:
                b[i][j] = -1

    for i in range(len(a)):
        for j in range(len(a[0])):
            if b[i][j] == 2:
                a[i][j] = 1
            elif b[i][j] == -1:
                a[i][j] = 0
    return a

def main():
    X,Y = 45,45
    a = [[random.randint(0, 1) for i in range(X)] for j in range(Y)]
    old_a = copy.deepcopy(a)
    itera = 0
    while True:
        system("clear")

        life(1,a)
        for ic, line in enumerate(a):
            print(" ".join(["\u001b[36m◘" if x else "\u001b[35m." for x in line]),end="\u001b[30m\n")
        itera += 1
        if itera%3==1:
            old_a = copy.deepcopy(a)
        elif a == old_a:
            time.sleep(1)
            break
        # print("\n")
        time.sleep(0.075)

    main()
main()