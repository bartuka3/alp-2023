#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import sys

# from numpy import full as np_arr
DEBUG = False


def debug(*args):
    if not DEBUG:
        return 1

    print(*args)


def get_matrix_of_signum(filename):
    """Takes in a filename and maps its values to sgn(x)"""
    b = []
    with open(filename, "r") as file:
        for line in file:
            b.append([True if int(x) < 0 else False for x in line.split()])
    return b


def get_rect_area(y1, x1, y2, x2):
    return (x2 - x1) * (y2 - y1) if (x2 - x1) != 0 else 1


def write_sum_matrix_into_board(board):
    """Creates a sum matrix"""
    for i, line in enumerate(board):  # we skip the first line
        if i:
            for j, element in enumerate(line):
                board[i][j] = bool(board[i][j]) * (board[i - 1][j] + 1)  # 0->0, 1->last row's value
        else:  # i == 0
            for j, element in enumerate(line):
                board[i][j] = int(board[i][j])
    return board


def max_of_histogram(histogram: list):
    """ Computes the maximum on a histogram, similarly to the assignment's resources
    Returns [j, j_sum], both ints
    """

    n = len(histogram)
    s_min = -1
    stack = [-1]
    l_s = [s_min] * n
    r_s = [n] * n

    for i in range(0, n):
        while stack:
            ss = stack[-1]
            if ss != -1 and histogram[ss] > histogram[i]:
                r_s[ss] = i
                stack.pop()
            else:
                break
        if i and histogram[i] == histogram[i - 1]:
            l_s[i] = l_s[i - 1]
        else:
            l_s[i] = stack[-1]
        stack.append(i)

    # We have to do this afterwards or we will use corrupted data
    area = [0, 0]
    cur_ar_max = 0
    for j, histo_val in enumerate(histogram):
        if histo_val * (r_s[j] - l_s[j] - 1) >= cur_ar_max:
            cur_ar_max = histo_val * (r_s[j] - l_s[j] - 1)
            area = [j, cur_ar_max, l_s[j], r_s[j]]
    return area


def find_smallest_i(histogram, point, direction=-1):
    """Goes to the left and returns when any value is smaller than our starting point"""
    acc = point
    point_value = histogram[point]
    while acc >= 0 and acc <= len(histogram):
        if histogram[acc] < point_value:  # we failed already
            return acc + 1
        acc += direction
    return 0  # everything to the left passes


def compute_biggest_rectangle_in_terms_of_number_of_elements_as_coordinates_in_matrix(board):
    max_vert = [0, 0, 0, 0, 0]
    for y, sum_line in enumerate(board):
        a = max_of_histogram(sum_line)
        j, area, ls, rs = a
        # print(x)
        if a[1] >= max_vert[-1]:
            max_vert = [y-sum_line[j]+1, ls+1, y, rs-1, area]
    return max_vert


def print_matrix(m):
    for r in m:
        for col in r:
            print(col, end=" ")
        print("")


def main():
    fn = sys.argv[1]
    # fn = "1_2_5_38.txt"
    # fn = "matrix_small_1.txt"
    # fn = "matrix_small_1.txt"
    # fn = "test_manual.txt"

    board = get_matrix_of_signum(fn)
    # IN PLACE
    write_sum_matrix_into_board(board)  # writes the sum matrix into board
    # print_matrix(board)
    # debug(board)
    # Histogram max rect

    a = compute_biggest_rectangle_in_terms_of_number_of_elements_as_coordinates_in_matrix(board)
    b = a[:-1]
    debug(a)
    print(*(b[:2]))
    print(*(b[2:]))
    return 0


if __name__ == "__main__":
    main()
