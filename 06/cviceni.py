# násobení maticí
aa = [[1, 1], [1, 1]]
bb = [[1, 2], [3, 4]]


def mm(a, b):
    if not (len(a) > 0 and len(b)) or len(a[0]) != len(b):
        return []
    res = [[0 for _ in range(len(b[0]))] for _ in range(len(a))]
    for r in range(len(a)):
        for c in range(len(b[0])):
            for i in range(len(a[0])):
                res[r][c] += a[r][i] * b[i][c]

    return res

