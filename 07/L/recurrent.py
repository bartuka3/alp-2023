#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

def run_w_cache(f, n, x):
    global POLE
    POLE = [None for _ in range(n+1)]
    return f(n, x)


def rec(n, x):
    if POLE[n]:
        return POLE[n]
    if not n:
        POLE[n] = -1
        return -1
    elif n == 1:
        POLE[n] = x
        return x
    elif n == 2:
        POLE[n] = -(x + 1) / 3
        return -(x + 1) / 3
    a = (n / x) * rec(n - 1, x) + ((-1) ** n) * (n + 1) / (n - 1) * rec(n - 2, x) + (n - 1) / (2 * x) * rec(n - 3, x)
    POLE[n] = a
    return a
def main():
    n = int(input())
    x = float(input())
    print(run_w_cache(rec,n,x))

if __name__ == "__main__":
    main()
