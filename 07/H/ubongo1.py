#  Copyright (c) 2023.
#  bartuka3, FEL ČVUT

def get_input():
    test = """5 5
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    -1 0 0 0 0
    """
    f = open("ubo.txt", "r")
    R, S = (int(c) for c in f.readline().split())
    board = []
    pieces = []
    for r in range(R):
        board.append(list(map(int, f.readline().split())))
    for line in f.readlines():
        l = list(map(int, line.split()))
        piece = []
        for i in range(len(l) // 2):
            piece.append([l[i * 2], l[i * 2 + 1]])
        pieces.append(piece)
    return board, pieces


def move_piece_to_center(p):
    np = []
    for cell in p:
        r, c = cell
        r -= p[0][0]
        c -= p[0][1]
        np.append([r, c])
    return np


def rotate(piece, rot):
    np = []
    for cell in piece:
        r, c = cell
        for rr in range(rot):
            r, c = -c, r
        np.append([r, c])
    return np
    pass


def inside(r, c, m):
    return r >= 0 and r < len(m) and c >= 0 and c < len(m[r])


def can_be_placed(piece, m, r, c):
    for cell in piece:
        r1, c1 = cell
        r1 += r
        c1 += c
        if inside(r1, c1, m):
            return m[r1, c1] == 0
        return False


def main():
    board, pieces = get_input()
    new_pieces = []
    for p in pieces:
        new_pieces.append(move_piece_to_center(p))
    print(new_pieces)

    def solve(n):
        nonlocal board, pieces, new_pieces
        valid = True
        if n == len(new_pieces):
            for r in range(len(board)):
                for c in range(len(board[r])):
                    valid = valid and board[r][c] != 0
            if valid:
                print("RESENI")
                print(board)
                quit()
        for p in new_pieces:
            for r in range(len(board)):
                for c in range(len(board[r])):
                    for rot in range(4):
                        rotp = rotate(p, rot)
                        if can_be_placed(rotp, board, r, c):
                            for cell in rotp:
                                r1, c1 = cell
                                board[r1 + r][c1 + c] = n + 1
                            solve(n + 1)
                            for cell in rotp:
                                r1, c1 = cell
                                board[r1 + r][c1 + c] = 0

    solve(0)


if __name__ == "__main__":
    main()
