#  Copyright (c) 2023.
#  bartuka3, FEL ČVUT
sys, ff, o = __import__("sys"), __import__("functools"), __import__("operator")

def not_placable(p, r, c, b):
    for cl in p:
        if not (0 <= cl[0] + r < len(b) and 0 <= cl[1] + c and cl[1] + c < len(b[0])) or b[cl[0] + r][cl[1] + c]:
            return True

def solve(b, n, pcs):
    if n == len(pcs):
        if ff.reduce(o.__and__, [ff.reduce(o.__and__, [bool(y) for y in b[x]]) for x in range(len(b))]):
            print("\n".join([" ".join(map(str, line)) for line in b]))
            sys.exit(0)
        return
    for c in range(len(b)):
        for r in range(len(b[0])):
            for rot in range(4):
                pcs[n] = [(-cell1[1], cell1[0]) for cell1 in pcs[n]]
                if not not_placable(pcs[n], r, c, b):
                    for cell in pcs[n]:
                        b[cell[0] + r][cell[1] + c] = n + 1
                    solve(b, n + 1, pcs)
                    for cell in pcs[n]:
                        b[cell[0] + r][cell[1] + c] = 0

def main():
    with open(sys.argv[1], "r") as file:  # get_file(), inlined
        board, pcl = [[int(x) for x in file.readline().split()] for _ in range(int(file.readline().split()[1]))], []
        for piece_line in file:
            l, p = list(map(int, piece_line.split())), []
            pcl.append([[l[i * 2], l[i * 2 + 1]] for i in range(len(l) // 2)])
            for p_i in range(1, len(pcl[-1])):
                pcl[-1][p_i] = [pcl[-1][p_i][0] - pcl[-1][0][0], pcl[-1][p_i][1] - pcl[-1][0][1]]
            pcl[-1][0] = [0, 0]
    solve(board, 0, pcl)
    print("NO_SOLUTION")

main()