def binomic_coef(k, n):
    if not k or k == n:
        return 1
    elif k == 1 or k == n - 1:
        return n
    return binomic_coef(k, n - 1) + binomic_coef(k - 1, n - 1)


# print(binomic_coef(3, 5))

POLE = {}


def f(n):
    if n in POLE:
        return POLE[n]
    if n in (0, 1):
        POLE[n] = n
        return n
    a = f(n - 1) + f(n - 2)
    POLE[n] = a
    return a


def flatten(a, b):
    for element in a:
        if type(element) == list:
            flatten(element, b)
        else:
            b.append(element)
    return b


def flatten_test():
    a = [1, 2, [3, [4, 5], 6, [7]], [8, 9], 10]
    b = []
    print(flatten(a, b))


def hanoi(n, src, dest, temp):
    if n < 1:
        return dest
    # 1.
    hanoi(n - 1, src, temp, dest)
    # 2. poslední ze src -> dest
    disc = src.pop()
    dest.append(disc)
    # 3.
    hanoi(n - 1, temp, dest, src)


def test_hanoi():
    a = [3, 2, 1]
    b = []
    c = []
    hanoi(3, a, c, b)
    print(c)


test_hanoi()
