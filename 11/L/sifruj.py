#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

def rychle_nasobeni(a,k):
    if not k: return 1
    elif k == 1: return a

    if k%2:
        return rychle_nasobeni(a,k/2)**2
    else:
        return a * rychle_nasobeni(a,k-1)


