#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT
MAX = 4, 7, 8, 10
SEARCH = 11


def state_pretty_print(l):
    for x in l:
        print(repr(x))


class State:
    def __init__(self, vol, action="S", prev=None, max_vol=MAX):
        # vol = [0, 0, 0]
        self.volumes = vol.copy()
        self.max_volumes = max_vol
        self.action = action
        self.prev = prev

    def __eq__(self, other):
        return self.state() == other.state()

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return " ".join(map(str, self.state())) + ": " + self.action

    def __str__(self):
        return ", ".join(map(str, self.state()))

    def state(self):
        return self.volumes

    def expansion(self):
        res = []
        # Nx
        for i in range(len(self.volumes)):
            # Nx
            if self.volumes[i] < self.max_volumes[i]:
                # Přeplnění
                vol = State(self.volumes, prev=self)
                vol.volumes[i] = self.max_volumes[i]
                vol.action = "N%s" % i
                vol.prev = self
                res.append(vol)
            # Vx
            if self.volumes[i]:
                vol = State(self.volumes, prev=self)
                vol.volumes[i] = 0
                vol.action = "V%s" % i
                res.append(vol)
            # xPy
            for j in range(len(self.volumes)):
                if j != i and self.volumes[i] and self.volumes[j] != self.max_volumes[j]:
                    vol = State(self.volumes, prev=self)
                    rest_j = self.max_volumes[j] - self.volumes[j]
                    if self.volumes[j] >= rest_j:
                        vol.volumes[i] -= rest_j
                        vol.volumes[j] += rest_j
                    else:
                        vol.volumes[j] += vol.volumes[i]
                        vol.volumes[i] = 0

                    vol.action = f"{i}P{j}"
                    res.append(vol)

        return res


def look_back(node):
    nodes = [node]
    while node.prev:
        nodes.append(node.prev)
        node = node.prev
    return nodes


def solve_bfs(start, searched=SEARCH):
    """Performs BFS
       Assumes that `start` has an expansion() function and
       that `start` is hashable.
    """
    q = [start]
    visited = {}
    while len(q):
        curr = q.pop(0)
        nn = curr.expansion()
        for act in nn:
            if str(act) not in visited:
                visited[str(act)] = 1
                q.append(act)
            if act.current_state[-1] == searched:
                return act


if __name__ == "__main__":
    a = State([0, 0, 0, 0])
    # print_list(a.expansion())
    print(" ".join(map(str, MAX)), "=>", SEARCH)
    state_pretty_print(reversed(look_back(solve_bfs(a, SEARCH))))
