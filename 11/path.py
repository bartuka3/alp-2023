#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

class State:
    def __init__(self, vol, action="", prev=None):
        # vol = [0, 0, 0]
        self.value = vol
        self.action = action
        self.prev = prev

    def __eq__(self, other):
        return self.state() == other.state()

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return " ".join(map(str, self.state()))  + ": " + self.action

    def __str__(self):
        return self.action

    def state(self):
        return [self.value]

    def expansion(self, graph_map):
        res = []
        for vertex in graph_map[self.value] if self.value in graph_map else []:
            res.append(State(vertex, "{0} - {1}".format(self.value,vertex), self))
        return res


def import_graph_as_edge_list(fn):
    a = {}
    with open(fn, "r") as file:
        for line in file:
            if not line:
                break
            vl = list(map(int, line.split()))
            # start, end - edge
            if vl[0] in a:
                a[vl[0]] = a[vl[0]] + [vl[1]]
            else:
                a[vl[0]] = [vl[1]]
    return a


def solve_bfs_graph(start, searched, graph_map):
    """Performs BFS in a graph
       Assumes that `start` has an expansion() function and
       that `start` is hashable.
    """
    q = [start]
    visited = {}
    while len(q):
        curr = q.pop(0)
        nn = curr.expansion(graph_map)
        for act in nn:
            if str(act) not in visited:
                visited[str(act)] = 1
                q.append(act)
            if act.value == searched:
                return act

def look_back(node):
    nodes = [node]
    while node.prev:
        nodes.append(node.prev)
        node = node.prev
    return nodes

def main():
    graph = import_graph_as_edge_list("path_test1.txt")
    start = 2
    end = 5
    f_state = State(start, str(start), None)
    finish_state = solve_bfs_graph(f_state,end,graph)
    print(" ".join([str(x.value) for x in reversed(look_back(finish_state))]) if finish_state else "None")


if __name__ == "__main__":
    main()
