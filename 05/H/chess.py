#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

from enum import Enum
from sys import exit,argv
from copy import deepcopy

class State(Enum):
    Mate = "MAT"
    Check = "SACH"
    Garde = "GARDE"
    Nothing = "NO"

def print_and_exit(string):
    print(string)
    exit(0)

"""
VSTUP:
 0  0  0 -3 -3  0 -1 -4
-6 -6 -2  0  5 -6  0  0
 0  0 -6  0  0  0  0  4
 0  0  0  0 -6  0  6  0
 0  0  0  0  4  0  0  0
 0  0  0  0  0  0  0  0
 6  6  6  0  2  0  0  0
 0  0  1  0  0  0  3  0


figura 	bílý 	černý
král 	1 	-1
dáma 	2 	-2
věž 	3 	-3
střelec 	4 	-4
jezdec 	5 	-5
pěšec 	6 	-6
volné pole 	0 	0 
"""


class FiguresW(Enum):
    Pawn = 6
    Knight = 5
    Bishop = 4
    Rook = 3
    Queen = 2
    King = 1

MOVEMENTS = { 1: ((1, 1),    (1, 0),    (1, -1), # KING
                   (0, 1),               (0, -1),
                   (-1, 1),   (-1, 0),   (-1, -1)),


   5:        ((2,1),(2,-1), # KNIGHT
        (1,2) ,          (1,-2),
        (-1,2),          (-1,-2),
             (-2,1),(-2,-1))

}
LINE_MOVEMENTS = {
    3: ((1, 0), (0, 1), (-1, 0), (0, -1)),  # ROOK
    4: ((1, 1), (-1, -1), (1, -1), (-1, 1)), # BISHOP
    2: ((1, 1), (-1, -1), (1, -1), (-1, 1), (1, 0), (0, 1), (-1, 0), (0, -1)) # QUEEN
}


def is_in_constraint(a: int, b: int, *args):
    """Checks if all numbers in args are within the constraint of a and b, alas,
    if a >= arg > b
    """
    for arg in args:
        # print(arg)
        if a > arg or arg >= b:
            # print("aaa",arg)
            return False
    return True


def ax(a: int, x: tuple):
    return tuple((a * xi for xi in x))


def parse_board_str(board_file):
    board = [0] * 8
    with open(board_file) as file:
        for i, line in enumerate(file):
            board[i] = [int(x) for x in line.split()]
    return board


def locate_all_player_positions(board, playing_color=1):
    """Player is actually an inverse of the argument"""
    can_move_positions = [[[(0, 0, 0)] for _ in range(8)] for _ in range(8)]

    for i, board_line in enumerate(board):  # board y (reversed)
        for j, board_piece in enumerate(board_line):  # board x

            # Let's use position tables
            if playing_color * board_piece in MOVEMENTS:
                for piece_position in MOVEMENTS[playing_color * board_piece]:  # applying the tables
                    # We need to stay on the board
                    if not (((i + piece_position[0]) in range(0, 8)) and (j + piece_position[1]) in range(0,
                                                                                                          8)): continue
                    if board[i + piece_position[0]][j + piece_position[1]] > 0: continue
                    can_move_positions[i + piece_position[0]][j + piece_position[1]].append((i, j, board_piece))

            elif board_piece == 6 * playing_color:  # Pawn handling
                if not (((i - 1) in range(0, 8)) and (j) in range(0, 8)): continue  # Invalid location
                if j != 7 and board[i - playing_color][j + 1] < 0:
                    can_move_positions[i - playing_color][j + 1].append((i, j, board_piece))
                if j != 0 and board[i - playing_color][j - 1] < 0:
                    can_move_positions[i - playing_color][j - 1].append((i, j, board_piece))

                if not board[i - playing_color][j]:  # Space right in front
                    can_move_positions[i - playing_color][j].append((i, j, board_piece))

                    if playing_color>0:  # White
                        if i == 6 and not (board[i - 2 * playing_color][j]):  # MAGIC: second row for white
                            can_move_positions[i - 2][j + 0].append((i, j, board_piece))
                    else:  # Black
                        if i == 1 and not (board[i - 2 * playing_color][j]):
                            can_move_positions[i + 2][j + 0].append((i, j, board_piece))

            # """ Rooks, Knights, Queen"""
            elif board_piece * playing_color in LINE_MOVEMENTS:
                for ii, vector in enumerate(
                        LINE_MOVEMENTS[playing_color * board_piece]):  # For all vectors from the table
                    iplus = 1
                    while True:  # We grab positions until we run out of them
                        a, b = ax(iplus, vector)
                        if not is_in_constraint(0, 8, i + a, j + b):  # We can't leave the board
                            break
                        elif board[i + a][j + b]:
                            if playing_color>0:  # White
                                if board[i + a][j + b] < 0:
                                    can_move_positions[i + a][j + b].append((i, j, board_piece))
                            else:  # Black
                                if board[i + a][j + b] > 0:
                                    can_move_positions[i + a][j + b].append((i, j, board_piece))
                            break  # In this direction the vector is already dead
                        else:
                            can_move_positions[i + a][j + b].append((i, j, board_piece))

                        iplus += 1
    # DEBUG PRINT
    return deepcopy(can_move_positions)


def debug_print(board, p_can_move_positions, customposition=(-1, -1)):
    return 0
    for i in range(8):
        print(" ".join(["\u001b[34m" + str(piece).zfill(2)
                        if (i, j) == customposition
                        else "\u001b[36m" + str(piece).zfill(2)
        if len(p_can_move_positions[i][j]) > 1
        else "\u001b[30m" + str(piece).zfill(2)
                        for j, piece in enumerate(board[i])]))
    print("\n")


def locate_pieces(board, type):
    locations = []
    for i, line in enumerate(board):
        for j, piece in enumerate(line):
            if piece == type:
                locations.append((i, j))
    return locations
    pass


def check_garde(board, white_positions):
    locations = locate_pieces(board, -2)  # Locate the queens
    for queen in locations:
        if len(white_positions[queen[0]][queen[1]]) > 1:
            print_and_exit("GARDE")
    return False
    pass


def check_check(board, white_positions):
    locations = locate_pieces(board, -1)  # Locate the queens
    for king in locations:
        if len(white_positions[king[0]][king[1]]) > 1:
            return king, white_positions[king[0]][king[1]]
    return False


def check_mate(board, white_positions, king_position, check_piece):
    king_x, king_y = king_position


    # KING MOVES
    for piece_position in MOVEMENTS[1]:  # NOTE White moves ~ black moves
        # Position needs to make sense
        if not is_in_constraint(0, 8, king_x + piece_position[0], king_y + piece_position[1]):
            continue
        elif board[king_x + piece_position[0]][king_y + piece_position[1]] < 0:
            continue  # position needs to be free / enemy

        elif not len(
                white_positions[king_x + piece_position[0]][king_y + piece_position[1]]) > 1:  # we can jump there and avoid mate
            temp = board[king_x + piece_position[0]][king_y + piece_position[1]]
            board[king_x + piece_position[0]][king_y + piece_position[1]] = -1
            board[king_x][king_y] = 0

            # debug_print(board, white_positions, (i + piece_position[0], j + piece_position[1]))

            if not check_check(board,locate_all_player_positions(board)):
                print_and_exit("SACH")
            else: #revert
                board[king_x][king_y] = -1
                board[king_x + piece_position[0]][king_y + piece_position[1]] = temp

    if len(white_positions[king_x][king_y]) > 3:  # We *likely* can't block 2+ pieces in one turn
        # print("too many")
        print_and_exit("MAT")
        return

    # MINOR PIECE MOVES
    attacking_pieces = white_positions[king_x][king_y][1:]
    a_p = deepcopy(attacking_pieces) # we can cut pieces that we can block
    black_positions = locate_all_player_positions(board, -1) # black movements
    for offending_piece in attacking_pieces:
        ii, jj = offending_piece[0:2]
        if len(black_positions[ii][jj])>1:
            # We virtually kill the piece, disregarding the original position
            board[ii][jj] = black_positions[ii][jj][1][-1]
            board[black_positions[ii][jj][1][0]][black_positions[ii][jj][1][1]] = 0
            if not check_check(board, locate_all_player_positions(board)):
                a_p.pop()
                break
            else: # We did not help anything and thus we revert
                board[ii][jj] = offending_piece[-1]
                board[black_positions[ii][jj][1][0]][black_positions[ii][jj][1][1]] = black_positions[ii][jj][1][-1]

        pass

    if len(a_p):
        #  line piece blocking
        for piece in a_p:
            # print(piece)
            if piece[-1] in (2,3):
                # lines
                pos_vector = (
                    ((-1) ** (1+int(piece[0] - king_position[0] > 0))) if piece[0] - king_position[0] else 0,
                    ((-1) ** (1+int(piece[1] - king_position[1] > 0))) if piece[1] - king_position[1] else 0)
                # debug_print(board,black_positions)
                iii = 1  # Incrementing vectors again
                black_played = False
                while is_in_constraint(0, 8, king_x + pos_vector[0]  * iii,  king_y + pos_vector[1] * iii)  and \
                        board[ king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] >= 0 :  # Place is
                    # empty
                    if black_played: break
                    if len(black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii]) > 1:
                        n = 1
                        if black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii][1][-1]==-1:
                            if len(black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii][1])>3:
                                n = 2
                        # Black can block
                        temp = board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii]
                        board[black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii][n][0]][
                            black_positions[king_x +
                                            pos_vector[0] * iii][king_y +
                                                                 pos_vector[1] * iii][n][1]] = 0
                        board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] = black_positions[king_x + pos_vector[0] *
                                                                                                  iii][king_y +
                                                                                                       pos_vector[1]
                                                                                                       * iii][n][-1]
                        if check_check(board, locate_all_player_positions(board)):  # Not good enough
                            iii += 1
                            black_played = True
                            board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] = temp
                            continue
                        else:  # We found a cover
                            # debug_print(board,locate_all_player_positions(board))
                            print_and_exit("SACH")
                            return
                    iii += 1
            if piece[-1] in (2, 4):  # diagonal pieces
                pos_vector = (
                (-1) ** (1 + int(piece[0] - king_position[0] > 0)), (-1) ** (1 + int(piece[1] - king_position[1] >
                                                                                     0)))
                iii = 1  # Incrementing vectors again
                black_played = False
                while not board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] < 0 and is_in_constraint(0, 8,
                                                                                                           king_x +
                                                                                                           pos_vector[0]
                                                                                                           * iii, king_y +
                                                                                                                  pos_vector[
                                                                                                                      1] * iii):  # Place is
                    # empty

                    if black_played: break
                    if len(black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii]) > 1:  # Black can block
                        temp = board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii]
                        board[black_positions[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii][1][0]][
                            black_positions[king_x +
                                                                                                          pos_vector[0] * iii][king_y +
                                                                                                               pos_vector[1] * iii][1][1]] = 0
                        board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] = black_positions[king_x + pos_vector[0] *
                                                                                                  iii][king_y +
                                                                                                       pos_vector[1]
                                                                                                       * iii][1][-1]
                        if check_check(board, locate_all_player_positions(board)):  # Not good enough
                            iii += 1
                            black_played = True
                            board[king_x + pos_vector[0] * iii][king_y + pos_vector[1] * iii] = temp
                            continue
                        else:  # We found a cover
                            # debug_print(board,locate_all_player_positions(board))
                            print_and_exit("SACH")
                            return
                    iii += 1
            else:  # We can't block other pieces
                print_and_exit("MAT")
                return
        print_and_exit("MAT")
    else:
        # print("Yay")
        print_and_exit("SACH")

    return False


def main():
    board = parse_board_str(argv[1])
    white_positions = locate_all_player_positions(board)
    is_check = check_check(board, white_positions)
    if is_check:
        check_mate(board, white_positions, *is_check)
    check_garde(board, white_positions)
    print_and_exit("NO")

    return 0


if __name__ == "__main__":
    main()
