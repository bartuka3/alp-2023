#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import sys

def read_matrix_from_file(filename: str):
    """Loads a 2D matrix from a filename"""
    m = []
    with open(filename,"r") as file:
        for line in file:
            m.append([int(x) for x in line.replace("\n","").split(" ")])
    return m

def print_matrix(m):
    for r in m:
        for col in r:
            print(col, end=" ")
        print("")


def mult_matrix_with_vector(m, v):
    res = [0] * len(m)
    if not len(m) or len(m[0]) != len(v): return []

    for r, line in enumerate(m):
        for c, el in enumerate(v):
            res[r] += line[c] * el

    return res


def main():
    m = [[1, 2], [3, 4]]
    v = [1, 1]
    print(mult_matrix_with_vector(m, v))

def main2():
    if len(sys.argv)>1:
        print_matrix(read_matrix_from_file(sys.argv[1]))
    else:
        print_matrix(read_matrix_from_file("matrix.txt"))
if __name__ == "__main__":
    main2()
