#!/bin/python3

# ALP 01:L Suma integerů 

# Vstup
a = int(input())
b = int(input())


suma = 0 # Akumulátor
if a<=b:
    for i in range(a,b+1): # Od a do b+1
        suma += i**3
else:
    for i in range(b,a+1): # Od b do a+1
        suma += i**3

# Výstup
print(suma)