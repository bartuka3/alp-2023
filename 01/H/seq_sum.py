#!/bin/python3
"""
01:H Suma posloupností
Hledá s následujícími podmínkami:
hledáme nejdelší klesající posloupnost NEprvočíselných čísel s největším součtem jejích členů  

Karel Bartůněk, 2022
bartuka3@fel.cvut.cz
"""

import sys


# Vstup
nums = list(map(int, input().split()))

# Prvočíselnost - Eratosthenovo síto
max_number = int(max([abs(n) for n in nums])) 
PRIME_NET = [True]*(max_number+1)


# Smažeme 0 a 1 nejsou prvočísly
PRIME_NET[0] = False
PRIME_NET[1] = False

for i in range(2,max_number): # Síto
    if not PRIME_NET[i]:
        continue
    j = 2 * i # Akumulátor násobků i, max
    while j <= max_number: # Posloupnost n*i, n ⊂ <2,[max/i]>
        PRIME_NET[j] = False
        j += i 
#DEBUG # print(len(PRIME_NET))

# Filtr prvočíselných hodnot
nums_prime = list(
    filter(
        lambda x: PRIME_NET[
            abs(x)
            ], # Zajímá nás prvočíselnost absolutní hodnoty čísla
        nums))
#DEBUG # print(nums)
#DEBUG # print(nums_prime)

if nums == nums_prime: #Není číslo, co splňuje podmínky prvočíselnosti
    print(0)
    print(0)
    sys.exit()


seq_length_store = [0]*len(nums) # Řetězcová délka pro každý element
for i,k in enumerate(nums):
    if not i:
        seq_length_store[i] = 0 # Číslo, nalezený nejdelší řetězec
        continue
    if nums[i-1] > k and not (k in nums_prime) and not (nums[i-1] in nums_prime):
        # Klesající posloupnost a nesmí být prvočíslo
        #DEBUG # print(k)
        seq_length_store[i] = seq_length_store[i-1]+1
    else:
        seq_length_store[i] = 0
    #DEBUG #print(i,k)

# Uložme si indície konců nejdelších řetězců
max_indices = [ind for ind, a in enumerate(seq_length_store) 
              if a == max(seq_length_store)]
#DEBUG # print(max_indices)

max_sums = [sum(nums[s-seq_length_store[s]:s+1]) for s in max_indices] # List sum nejdelších řetězců

#DEBUG # print(max_sums)

#DEBUG # print("----")
# RETURN
print(seq_length_store[max_indices[max_sums.index(max(max_sums))]]+1) # Voláme zpátky a chceme délku řetězce
print(max(max_sums))