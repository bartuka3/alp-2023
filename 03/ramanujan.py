

# a^3 + b^3 = c
def ramanujan():
    for c in range(1000,10000):
        max_a = int(c**(1/3))+1
        counter = 0
        for a in range(max_a):
            b = int((c-a**3)**(1/3))+1
            if a**3+b**3 == c and a<b:
                counter += 1
                print(a,b,"=",c)
                if counter > 1:
                    print ("NÁLEZ"+"\n"*4)
                    break


if __name__ == "__main__":
    ramanujan()


    # 2, 16, 4104
    # 9, 15, 4104

    # 1, 12, 1729
    # 9, 10, 1729