def shift(letter, n):
    l2 = ord(letter) + n
    if letter.isupper():
        if l2 > ord("Z"):
            l2 -= 26
        if l2 < ord("A"):
            l2 += 26
    else:
        if l2 > ord("z"):
            l2 -= 26
        if l2 < ord("a"):
            l2 += 26
    return chr(l2)


def caesar(strng,n):
    n = n%26
    result = []
    for let in strng:
        new = shift(let,n)
        result += new
    return "".join(result)

def main():
    print(caesar("FeL",5)))