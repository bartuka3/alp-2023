"""Date.py - creates lingual representations of numeral dates and vice versa
bartuka3, 2022
"""

import sys


def raise_error():
    print("ERROR")
    sys.exit()


ENGLISH_DATE_LITERALS = {
    "numbers": {
        0: "", 1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 6: "six", 7: "seven",
        8: "eight", 9: "nine", 10: "ten", 11: "eleven", 12: "twelve", 13: "thirteen",
        15: "fifteen", 16: "sixteen", 17: "seventeen", 18: "eighteen", 19: "nineteen",
        20: "twenty", 30: "thirty", 40: "forty", 50: "fifty", 80: "eighty", 100: "hundred", 1000: "thousand"
    },
    "days": {
        1: "first",
        2: "second",
        3: "third",
        5: "fifth",
        8: "eighth",
        9: "ninth",
        10: "tenth",
        11: "eleventh",
        12: "twelfth",
        13: "thirteenth",
        18: "eighteenth",
        20: "twentieth",
        21: "twenty-first",
        22: "twenty-second",
        23: "twenty-third",
        30: "thirtieth",
        31: "thirty-first"

    },
    "months": {
        1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June", 7: "July",
        8: "August", 9: "September", 10: "October", 11: "November", 12: "December"
    }
}

"""CREATE DATA"""
for i in range(3, 30):
    if not (i in ENGLISH_DATE_LITERALS["days"]):  # Do not override precalculated
        if i < 10:  # 3 - 9
            ENGLISH_DATE_LITERALS["days"][i] = ENGLISH_DATE_LITERALS["numbers"][i] + "th"
        elif i < 20:  # 11 - 19
            if not (i in ENGLISH_DATE_LITERALS["numbers"]):
                ENGLISH_DATE_LITERALS["numbers"][i] = ENGLISH_DATE_LITERALS["numbers"][i % 10] + "teen"
            ENGLISH_DATE_LITERALS["days"][i] = ENGLISH_DATE_LITERALS["numbers"][i] + "th"

        else:  # 20-29
            ENGLISH_DATE_LITERALS["days"][i] = "twenty-" + ENGLISH_DATE_LITERALS["days"][i % 20]

for i in range(13, 99 + 1):
    if not (i in ENGLISH_DATE_LITERALS["numbers"]):
        if not i % 10:
            ENGLISH_DATE_LITERALS["numbers"][i] = ENGLISH_DATE_LITERALS["numbers"][i // 10] + "ty"
        else:
            ENGLISH_DATE_LITERALS["numbers"][i] = ENGLISH_DATE_LITERALS["numbers"][(i // 10) * 10] + \
                                                  ENGLISH_DATE_LITERALS["numbers"][i % 10]


    def find_int_from_str(string):
        result_int = 0
        string = string
        find_10s = 0
        for tens in range(20, 100, 10):
            find_10s = string.find(ENGLISH_DATE_LITERALS["numbers"][tens])
            if find_10s >= 0:
                if find_10s:
                    raise_error()
                result_int += tens
                break
        if result_int:
            string = string[find_10s + len(ENGLISH_DATE_LITERALS["numbers"][result_int]):]
        for ii in range(1, 20):
            # print(ENGLISH_DATE_LITERALS["numbers"][ii])

            if string == ENGLISH_DATE_LITERALS["numbers"][ii]:
                result_int += ii
                # print(ii)
                break

        # sanity check
        if string and not (result_int % 10) and result_int!=10:
            # print(string,result_int % 10) # we have likely received a malformed number
            raise_error()

        return result_int


    def year_parser(yearstr=None):
        yearint = 0
        newstr = None
        find_1000 = yearstr.find(ENGLISH_DATE_LITERALS["numbers"][1000])  # check for a thousand
        if find_1000 == 0:
            raise_error()  # Starts with the word thousand
        elif find_1000 > 0:
            yearint += 1000 * find_int_from_str(yearstr[:find_1000])
            yearstr = yearstr[find_1000 + len(ENGLISH_DATE_LITERALS["numbers"][1000]):]  # cut thousands
        find_100 = yearstr.find(ENGLISH_DATE_LITERALS["numbers"][100])  # check for a hundred
        if find_100 == 0:  # Starts with the word hundred
            raise_error()
        elif find_100 > 0:
            yeartens = find_int_from_str(yearstr[:find_100])
            yearint += 100 * yeartens
            if yeartens > 10: raise_error()  # Cannot be eg. twelvehundred
            yearstr = yearstr[find_100 + len(ENGLISH_DATE_LITERALS["numbers"][100]):]  # cut hundreds
        # we have only the number to find, let's convert it as well
        if newstr:
            yearint += find_int_from_str(newstr)
        else:
            yearint += find_int_from_str(yearstr)
        # if we didn't error out, we are confident we have the entire year now, let's save it
        return yearint


class WrittenDate:
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year
        self.validate_internal_date()

    @classmethod
    def from_literal(cls, date_str):
        """29.2.1234 --> the twenty-nineth of February onethousandtwohundredthirtyfour"""
        try:
            return cls(*[int(x) for x in date_str.split(".")])
        except ValueError:
            raise_error()
        pass

    @classmethod
    def from_text(cls, date_list: list):
        """the twenty-ninth of February onethousandtwohundredthirtyfour --> 29.2.1234"""
        day = date_list[1]
        month = date_list[3]
        year = date_list[4]

        all_days = {v: k for k, v in ENGLISH_DATE_LITERALS["days"].items()}
        all_months = {v: k for k, v in ENGLISH_DATE_LITERALS["months"].items()}

        if day in all_days:  # check days
            day_int = all_days[day]
        else:
            raise_error()
            return
        if month in all_months:  # Check months
            month_int = all_months[month]
        else:
            raise_error()
            return
        year_int = year_parser(yearstr=year)
        return cls(day_int, month_int, year_int)

    def to_literal(self):
        """Date --> xx.yy.zzzz"""
        return ".".join([str(x) for x in (self.day, self.month, self.year)])
        pass

    def to_text(self):
        out = []
        out.append("the")
        # day
        out.append(self.day_printer())
        # month
        out.append("of")
        out.append(ENGLISH_DATE_LITERALS["months"][self.month])
        # year
        out.append(self.year_printer())

        return " ".join(out)

    def day_printer(self):
        day_outstr = []
        # print(ENGLISH_DATE_LITERALS["days"][self.day])
        if self.day in ENGLISH_DATE_LITERALS["days"]:  # Precalculated
            return ENGLISH_DATE_LITERALS["days"][self.day]
        else:  # Combine
            day_outstr.append(ENGLISH_DATE_LITERALS["numbers"][((self.day // 10) * 10)])  # We retireve the tens (20,30)
            day_outstr.append("-")
            day_outstr.append(ENGLISH_DATE_LITERALS["days"][self.day % 10])  # Sigle days (fifth, eighth)
            return "".join(day_outstr)

    def year_printer(self):
        """Enters an int and attempts to construst an accurate year-string"""
        outstr = []
        # 1. thousands
        if self.year // 1000:
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][self.year // 1000])
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][1000])
        # 2. hundreds
        if (self.year % 1000) // 100:  #
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][(self.year % 1000) // 100])
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][100])
        # 3. check teens and single numbers
        if (self.year % 100) in ENGLISH_DATE_LITERALS["numbers"]:
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][self.year % 100])
        else:  # 3.1 Combined tens (20-99)
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][((self.year % 100) // 10) * 10])
            outstr.append(ENGLISH_DATE_LITERALS["numbers"][self.year % 10])

        return "".join(outstr)

    def validate_internal_date(self):
        """Validates the entered date"""
        self.is_valid_month()
        self.is_valid_day_of_month()
        self.is_valid_year()

    def is_valid_day_of_month(self):
        val = 31
        if not self.day:
            raise_error()
        if self.month == 2:
            val = 29
        elif self.month in (4, 6, 9, 11):  # sudé měsíce - 30 dní
            val = 30
        if self.day > val:
            raise_error()
        else:
            return

    def is_valid_month(self):
        if self.month > 12 or 0 >= self.month: raise_error()

    def is_valid_year(self):
        if not (self.year in range(1, 10000)): raise_error()


def detect_format(date_str: str) -> str:
    """Handles the conversion."""
    if date_str.count(".") == 2:  # Možná datum
        date_obj = WrittenDate.from_literal(date_str)
        return date_obj.to_text()
    else:  # Možná text
        date_str_split = date_str.split(" ")
        if date_str_split[0] == "the" and date_str_split[2] == "of":  # Splňuje formát, možná
            date_obj = WrittenDate.from_text(date_str_split)
            return date_obj.to_literal()

        # Tak už opravdu nevím, co to je.
        else:
            raise_error()


def main():
    date_str = input()
    # DEBUG
    # date_str = "the twenty-eighth of April sixthousandninehundredtwentyeight" # 28.4.6928
    # date_str = "31.11.123" # --> ERROR
    # date_str = "18.11.2824" # --> the eighteenth of November twothousandeighthundredtwentyfour
    #
    # date_str = "the ninth of March ninethousandsixhundredfortysix" # 9.3.9646
    # date_str = "the thirtieth of April twentytwohundredthirtyfour" # ERROR
    #
    # date_str = "13.6.8849" # the thirteenth of June eightthousandeighthundredfortynine
    # date_str = "the thirteenth of June fourthousandthreehundredeightyseven" # 13.6.4387
    # date_str = "the first of January onehundreddtwentythree" # ERROR
    #
    # date_str = "15.7.1752" # the fifteenth of July onethousandsevenhundredfiftytwo
    # date_str = "the fifteenth of August seventhousandfivehundredsixty" # 15.8.7560
    # date_str = "the thirtieth of September ten" # 30.9.10

    date_new = detect_format(date_str)
    print(date_new)


if __name__ == "__main__":
    main()
