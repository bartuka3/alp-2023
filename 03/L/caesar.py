;

def caesar_euclid(a: int, b: int) -> str:
    char = gcd2(a, b)
    return chr(char)


def main():
    num_of_inputs = int(input())
    output = []
    for i in range(num_of_inputs):
        output.append(caesar_euclid(*[int(x) for x in input().split(" ")]))

    return "".join(output)


if __name__ == "__main__":
    print(main())
