def cas_to_seconds(h,m,s):
    return h*3600+m*60+s

def seconds_to_cas(s):
    return "{}:{}:{}".format(s//3600,s%60-s//3600,s-s%60-s//3600)

cas_sekundy = 10000

cas_1 = "6:10:59"
cas_2 = "13:12:01"

cas_1_l = [int(x) for x in cas_1.split(":")]
cas_2_l = [int(x) for x in cas_2.split(":")]


cas_1_s = cas_to_seconds(*cas_1_l)
cas_2_s = cas_to_seconds(*cas_2_l)

print(cas_1_s-cas_2_s)
print(seconds_to_cas(cas_1_s-cas_2_s))


