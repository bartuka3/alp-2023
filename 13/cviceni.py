def get_parent(index):
    return (index - 1) // 2


def get_children(index):
    return int((index * 2 + 1))


def is_in(a, b, x):
    return a < x < b


class MinHeap:
    def __init__(self):
        self.heap = []
        pass

    def __str__(self):
        return str(self.heap)

    def __repr__(self):
        return str(self.heap)

    def add(self, value):
        self.heap.append(value)
        self.bubble_up(len(self.heap) - 1)
        pass

    def pop(self, index):
        self.heap[index], self.heap[-1] = self.heap[-1], self.heap[index]
        val = self.heap.pop()
        self.bubble_down(index)
        return val

    def bubble_up(self, index):
        """Bubbles an element up to preserve the characteristics of MinHeap
        after an element was inserted.
        """
        while index > 0:
            p = get_parent(index)
            if self.heap[p] > self.heap[index]:
                self.heap[p], self.heap[index] = self.heap[index], self.heap[p]
            else:
                break
            index = p
        return

    def bubble_down(self, index):
        while index < len(self.heap):
            left = get_children(index)
            right = left + 1

            if is_in(-1, len(self.heap), left) and self.heap[index] > self.heap[left]:
                self.heap[index], self.heap[left] = self.heap[left], self.heap[index]
                index = left
            elif is_in(-1, len(self.heap), right) and self.heap[left] > self.heap[right]:
                self.heap[index], self.heap[right] = self.heap[right], self.heap[index]
                index = right
            else:
                break
        return


if __name__ == "__main__":
    mh = MinHeap()
    for i in [2, 3, 4, 5, 1, 8, 99]:
        mh.add(i)
    mh.pop(4)
    print(mh)
