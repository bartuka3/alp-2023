#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import sys


def f(x, y):
    return (1 / 2) * (x ** 2) * ((1 - y) ** 2) + ((x - 2) ** 3) - 2 * y + x


def g(x, y):
    return (x + 2) * (y - 2)


def load_line():
    data = list(map(float, input().split()))
    return data


def main():
    Xline = load_line()
    Yline = load_line()
    if len(Xline) != len(Yline):
        print("ERROR")
        return
    res = [f(x, y) for x, y in zip(Xline, Yline)]
    mval = max(res)
    mindex = res.index(mval)
    res2 = [g(x, y) for x, y in zip(Xline, Yline)]
    for i, el in enumerate(res2):
        res2[i] = res2[i] * res[i]
    minval = min(res2)
    minindex = res2.index(minval)
    negcount = list(map(lambda x: x < 0, res)).count(True)

    print(mindex, negcount, minindex)

    return 0


if __name__ == "__main__":
    main()
