#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import copy
import time
from _operator import add
from operator import add

import base
import random

EMPTY_CHAR = " "
random.seed(time.time_ns())

"""
Plagiátorství škodí! Před zneužitím tohoto programu k trénování BRUTE si prosím uvědomte čas strávený psaním
tohoto programu!

PS: Neočekávejte prosím, že tento kód je jakkoliv kvalitní a optimalizovaný
"""




# implement you player here. If you need to define some classes, do it also here. Only this file is to be submitted to Brute.
# define all functions here
def pretty_print_08(board):
    for line in board:
        print(" ".join([str(x) for x in line]))


class Player(base.BasePlayer):
    def __init__(self, board, name, color, algname="", e=4, m=11):
        self.EARLY_GAME = e
        self.MIDGAME = m
        self.EMPTY_CHAR = " "
        base.BasePlayer.__init__(self, board, name, color)
        self.algorithmName = """RGN"""

        if algname:
            self.algorithmName = algname
        self.edge_hashtable = dict()
        self.NEIGHBOURS = (
            (0, -1),
            (-1, 0),
            (0, 1),
            (1, 0))
        self.NEIGH_NUM = len(self.NEIGHBOURS)
        self.board_y = len(self.board)
        self.board_x = len(self.board[0])

        # I HATE THIS TIME LIMIT

        self.moves_hashtable = {'  d ': ['lldd', 'lddl', 'dldl'], '   l': ['ddll', 'lddl', 'dldl'],
                                'l   ': ['lldd', 'lddl', 'ldld'], ' d  ': ['ddll', 'lddl', 'ldld'],
                                '  l ': ['dlld', 'ddll', 'ldld'], '   d': ['lldd', 'dlld', 'ldld'],
                                ' l  ': ['lldd', 'dlld', 'dldl'], 'd   ': ['dlld', 'ddll', 'dldl'],
                                'l d ': ['lldd', 'lddl'], 'l  l': ['lddl'], ' ld ': ['lldd', 'dldl'],
                                'd  l': ['ddll', 'dldl'], '  dd': ['lldd'], 'ld  ': ['lddl', 'ldld'], 'dd  ': ['ddll'],
                                '  dl': ['lddl', 'dldl'], '  ll': ['ddll'], ' dd ': ['lddl'], ' dl ': ['ddll', 'ldld'],
                                ' ll ': ['dlld'], 'd  d': ['dlld'], 'l  d': ['lldd', 'ldld'], 'd dl': ['dldl'],
                                'd ll': ['ddll'], '  ld': ['dlld', 'ldld'], ' l d': ['lldd', 'dlld'], 'dd l': ['ddll'],
                                'dl l': ['dldl'], 'll  ': ['lldd'], ' d d': ['ldld'], 'd l ': ['dlld', 'ddll'],
                                ' dld': ['ldld'], ' lld': ['dlld'], ' ddl': ['lddl'], ' ldl': ['dldl'],
                                ' d l': ['ddll', 'lddl'], 'ldd ': ['lddl'], 'ldl ': ['ldld'], 'dl  ': ['dlld', 'dldl'],
                                'ld l': ['lddl'], 'ld d': ['ldld'], 'l ld': ['ldld'], 'l dd': ['lldd'],
                                'll d': ['lldd'], ' l l': ['dldl'], 'lld ': ['lldd'], 'dld ': ['dldl'],
                                'ddl ': ['ddll'], 'l l ': ['ldld'], 'd d ': ['dldl'], 'dl d': ['dlld'],
                                'd ld': ['dlld'], 'l dl': ['lddl'], 'lldd': ['lldd'], 'dlld': ['dlld'],
                                ' ldd': ['lldd'], 'dll ': ['dlld'], ' dll': ['ddll']}

        """ weights:
                 loops (completed), lines (completed, y), lines (completed, x), length (y), length(x) 
        """
        self.moves = 0
        self.weights_m = [2, 1, 1, 1, 1, 0, 0.0000000001]
        self.weights_o = [2, 1, 1, 1, 1, 0, 0.0000000001]
        self.MAXITER = 75
        self.DEPTH = 4
        self.total_score = 0

    def _inside(self, r, c):
        """ return True if cell [r][c] is inside the board """
        return r >= 0 and r < self.board_y and c >= 0 and c < self.board_x

    def find_and_murder(self):
        pass

    def find_first_valid_tile(self):
        # Find the first non-null card
        for ii in range(self.board_y):
            for jj in range(self.board_x):
                if self.board[ii][jj] != "none":
                    return ii, jj


    def get_board_copy(self):
        return copy.deepcopy(self.board)

    @staticmethod
    def add_2vector(v, w):
        return tuple(map(add, v, w))

    def mul_2vector(self, v, w):
        return tuple(x * y for x, y in zip(v, w))


    def _look_for_neighbours_edgesearch(self, tile):
        visited = [[True for _ in range(self.board_x)] for _ in range(self.board_y)]
        edges = []
        to_visit_stack = [tile]

        while to_visit_stack:
            t = to_visit_stack.pop()
            for neigh in self.NEIGHBOURS:
                coords = tuple(map(add, t, neigh))
                if self._inside(*coords) and visited[coords[0]][coords[1]]:  # valid position & not visited
                    if self.board[coords[0]][coords[1]][0] == "n":  # none
                        edges.append(coords)
                        visited[coords[0]][coords[1]] = False
                    else:
                        visited[coords[0]][coords[1]] = False
                        to_visit_stack.append(coords)

        return edges

    def GEN_look_for_neighbours_edgesearch(self, tile):
        visited = [[True for _ in range(self.board_x)] for _ in range(self.board_y)]
        c = False
        to_visit_stack = [tile]

        while to_visit_stack:
            t = to_visit_stack.pop()
            for neigh in self.NEIGHBOURS:
                coords = tuple(map(add, t, neigh))
                if self._inside(*coords) and visited[coords[0]][coords[1]]:  # valid position & not visited
                    if self.board[coords[0]][coords[1]][0] == "n":  # none
                        c = True
                        yield coords
                        visited[coords[0]][coords[1]] = False
                    else:
                        visited[coords[0]][coords[1]] = False
                        to_visit_stack.append(coords)

        return c

    def generate_hints_1move(self, pos):
        tip = [self.EMPTY_CHAR] * self.NEIGH_NUM  # empty hint
        for i, neighbour in enumerate(self.NEIGHBOURS):
            y, x = map(add, pos, neighbour)  # pos + neighbout
            if 0 <= y < self.board_y and 0 <= x < self.board_x:
                neigh_piece = self.board[y][x]
                if not neigh_piece[0] == "n":  # not none
                    # tip[i] = neigh_piece[(i + 2) % self.NEIGH_NUM]
                    tip[i] = neigh_piece[self.my2other[i]]
        return tip

    def generate_hints_from_movelist(self, positions):
        """Looks for neighbours around itself"""
        tip_list = []
        for p_i, pos in enumerate(positions):
            tip_list.append(self.generate_hints_1move(pos))
        return tip_list

    def get_moves_for_1_hint(self, hint):
        nh = "".join(hint)
        if nh in self.moves_hashtable:
            return self.moves_hashtable[nh]
        else:

            p = []
            for mm in self.tiles:
                possible = True
                for i in range(len(hint)):
                    if hint[i] != EMPTY_CHAR and hint[i] != mm[i]:
                        possible = False
                if possible:
                    p.append(mm)
            self.moves_hashtable[nh] = p.copy()
            return p

    def generate_all_moves_from_tips(self, positions, tips):
        """Input: tips on position
            Output: all possible tiles
        """

        all_positions = []
        for i, pos in enumerate(positions):
            for tile in self.get_moves_for_1_hint(tips[i]):
                all_positions.append((*pos, tile))
        return all_positions

    def clean_up_moves(self, movelist):
        """OVERRIDES EVERYTHING IN MOVELIST
            to none in THE BOARD
        """
        for mm in movelist:
            self.board[mm[0]][mm[1]] = "none"

    def mess_up_moves(self, movelist):
        """OVERRIDES EVERYTHING IN MOVELIST
            to MOVE in THE BOARD
        """
        for mm in movelist:
            self.board[mm[0]][mm[1]] = mm[2]


    def validate_and_generate_forced_moves_1_piece(self, move):
        all_moves = []
        valid = True
        visited = [[False for _ in range(self.board_x)] for _ in range(self.board_y)]

        def a(curr_move, rec=True):
            nonlocal visited
            nonlocal all_moves
            nonlocal valid
            for neigh in self.NEIGHBOURS:
                v = (curr_move[0], curr_move[1])
                y, x = tuple(map(add, v, neigh))
                if valid and 0 <= y < self.board_y and 0 <= x < self.board_x and (not visited[y][x]) and \
                        self.board[y][x][0] == "n":  # empty
                    # existing
                    # tile
                    hint = self.generate_hints_1move((y, x))

                    has_2 = False
                    for color in ("d", "l"):
                        c = hint.count(color)
                        if c > 2:  # illegal move would occur
                            valid = False
                            self.clean_up_moves(all_moves)
                            return None
                        elif c == 2:
                            has_2 = True
                    if has_2:
                        m_v = self.get_moves_for_1_hint(hint)
                        if [y, x, m_v[0]] not in all_moves:  # prevent duplicates
                            all_moves.append([y, x, m_v[0]])
                        self.board[y][x] = m_v[0]  # WRITES BOARD
                        visited[y][x] = True
                        a((y, x))

            return all_moves

        v = a(move)
        if v == None:
            pass
        return all_moves, valid


    @staticmethod
    def get_random_tile(moves):
        # return 0
        return moves[random.randint(0, len(moves) - 1)]

    def walk_for_cycles_1move(self, pos, color):
        fill_map = [[0 for _ in range(self.board_x)] for _ in range(self.board_y)]
        board_x = self.board_x
        board_y = self.board_y
        maxy, maxx = pos
        miny, minx = pos
        to_walk_stack = [pos]
        stroke_len = 1
        x_complete = 0
        y_complete = 0
        while to_walk_stack:
            pos = to_walk_stack[0]
            to_walk_stack = to_walk_stack[1:]
            for neigh in self.NEIGHBOURS:
                v = (pos[0], pos[1])
                y, x = tuple(map(add, v, neigh))
                pos_piece = self.board[pos[0]][pos[1]]
                if 0 <= y < self.board_y and 0 <= x < self.board_x:
                    neigh_piece = self.board[y][x]
                    if self.can_be_connected(neigh_piece, pos_piece, neigh, (y, x), color):
                        if x == 0 and neigh_piece[0] == color:
                            x_complete |= 1
                        if x == board_x - 1 and neigh_piece[2] == color:
                            x_complete |= 2
                        if y == 0 and neigh_piece[1] == color:
                            y_complete |= 1
                        if y == board_y - 1 and neigh_piece[3] == color:
                            y_complete |= 2
                        miny = min(miny, y)
                        minx = min(minx, x)
                        maxy = max(maxy, y)
                        maxx = max(maxx, x)
                        if fill_map[y][x]:
                            if abs(stroke_len - fill_map[y][x]) > 1:
                                return (stroke_len * (0.5 + (
                                    self.board[y - neigh[0]][x - neigh[1]] != "none"))), 0, abs(
                                    maxy - miny), abs(maxx - minx), y_complete, x_complete
                        else:
                            fill_map[y][x] = stroke_len
                            stroke_len += 1
                            to_walk_stack.append((y, x))
                else:
                    pass
        return 0, stroke_len, abs(maxy - miny), abs(maxx - minx), y_complete, x_complete

    def can_be_connected(self, neighbour, piece, neigh, pos, color):
        """Tells us whether or not we can join with the neighbour"""
        neighbour_dy, neighbour_dx = neigh
        assert neighbour_dy or neighbour_dx
        if neighbour == "none":  # Cannot connect to none
            neighbour = self.generate_hints_1move(pos)
        if neighbour == "none":
            return False
        if neighbour_dy:
            if neighbour_dy == 1:  # 1 0 --> 3 1
                return piece[3] == neighbour[1] and piece[3] == color
            else:  # -1 0 --> 1 3
                return piece[1] == neighbour[3] and piece[1] == color
        else: 
            if neighbour_dx == 1:  # 0 1 --> 2 0
                return piece[2] == neighbour[0] and piece[2] == color
            else:  # 0 -1 --> 0 2:
                return piece[0] == neighbour[2] and piece[0] == color


    def rank_board_1move_1color(self, pos, color, forced_moves):
        y, x = pos
        loop_score, stroke_len, ysize, xsize, y_complete, x_complete = self.walk_for_cycles_1move((y, x), color)
        ybonus = int((ysize >= (self.board_y - 2)) * (stroke_len if stroke_len else loop_score))
        xbonus = int((xsize >= (self.board_x - 2)) * (stroke_len if stroke_len else loop_score))
        yb = (
                (y_complete & 1 + ((y_complete & 2) >> 1)) + 4 * (y_complete & 1 * ((y_complete & 2) >> 1)))
        xb = (
                (x_complete & 1 + ((x_complete & 2) >> 1)) + 4 * (y_complete & 1 * ((y_complete & 2) >> 1)))
        nyb = (1 - 2*(bool(yb) and bool(xb))) * (yb - (yb & 4) + bool(yb & 4)) * (stroke_len if stroke_len else
            loop_score)
        nxb = (1 - 2*(bool(xb) and bool(yb))) * (xb - (xb & 4) + bool(xb & 4)) * (stroke_len if stroke_len else
                                                                                loop_score)
        loop_score_new = loop_score * (0.5 + loop_score > 4) * (not yb) * (not xb)
        v = (loop_score_new, ybonus, xbonus, nyb, nxb, len(forced_moves), stroke_len if stroke_len else loop_score)
        if color == self.myColor:
            return sum(tuple(x1 * y1 for x1, y1 in zip(v, self.weights_m)))
        else:
            return sum(tuple(x1 * y1 for x1, y1 in zip(v, self.weights_o)))

    def rank_board_1move(self, pos, mv):
        y, x = pos
        # 1. self
        score = 0

        score += self.rank_board_1move_1color(pos, self.myColor, mv)
        score -= self.rank_board_1move_1color(pos, self.rivalColor[self.myColor], mv)
        return score

    def alphabeta(self, depth, alpha, beta, is_max, curr_score=0, happened=()):
        global b
        global a
        mov = []
        if not depth or b > self.MAXITER * a * depth:
            return curr_score, mov
        elif not happened:
            a += self.board_y
            moves = self.GEN_expand()
        else:
            moves = self.GEN_expand_with_global_help(happened)

        if is_max:  # max player
            value = float("-inf")
            for m in moves:
                self.board[m[0]][m[1]] = m[2]  # imaginary
                # this generates imaginary pieces
                mv, valid = self.validate_and_generate_forced_moves_1_piece(m)
                self.mess_up_moves(mv)
                if valid:
                    b += 1
                    curr_score += self.rank_board_1move((m[0], m[1]), mv)
                    nv, _ = self.alphabeta(depth - 1, alpha, beta, not is_max, curr_score, [m] + [*mv])
                    value = max(value, nv)
                self.clean_up_moves(mv)  # remove imaginary
                self.board[m[0]][m[1]] = "none"
                # a -- b
                if valid:
                    if value >= beta:
                        break
                    if value > alpha:
                        alpha = value
                        mov = [[*m], *mv]
            if not mov:
                return curr_score, mov
            return value, mov
        else:  # min player
            value = float("inf")
            for m in moves:
                # print(moves)
                self.board[m[0]][m[1]] = m[2]  # imaginary
                # this generates imaginary pieces
                mv, valid = self.validate_and_generate_forced_moves_1_piece(m)
                self.mess_up_moves(mv)
                if valid:
                    b += 1
                    curr_score -= self.rank_board_1move((m[0], m[1]), mv)
                    nv, _ = self.alphabeta(depth - 1, alpha, beta, not is_max, curr_score, [m] + [*mv])
                    value = min(value, nv)
                self.clean_up_moves(mv)
                # remove imaginary

                self.board[m[0]][m[1]] = "none"
                # a -- b
                if valid:
                    if value <= alpha:
                        break
                    if value < beta:
                        beta = value
                        mov = [[*m], *mv]

            # print(mov)
            if not mov:
                return curr_score, mov
            return value, mov

    @staticmethod
    def make_board_str(board: list):
        return "".join(("".join(line)) for line in board)

    def expand(self):
        pos = self._look_for_neighbours_edgesearch(self.find_first_valid_tile())
        hint_list = self.generate_hints_from_movelist(pos)
        self.global_help = self.generate_all_moves_from_tips(pos, hint_list)
        return self.global_help

    def GEN_expand(self):
        c = False
        self.global_help = []
        for pos in self.GEN_look_for_neighbours_edgesearch(self.find_first_valid_tile()):
            hint = self.generate_hints_1move(pos)
            for m in self.get_moves_for_1_hint(hint):
                c = True
                self.global_help.append((*pos, m))
                yield (*pos, m)
        return c

    def expand_with_global_help(self, happened):
        """Assumes the board has been already expanded at least once"""
        if not len(happened):
            print("wtf")
            return self.expand()
        mov_list = []
        new_glob = self.global_help
        # all empty positions
        for new_piece in happened:
            # new_glob.remove(new_piece)
            for neigh in self.NEIGHBOURS:
                pos = new_piece[:2]
                v = new_piece[:2]
                y, x = tuple(map(add, v, neigh))
                if y >= 0 and y < self.board_y and x >= 0 and x < self.board_x and self.board[y][x][0] == "n":
                    if (y, x) not in self.global_help:
                        mov_list.append((y, x))

        hint_list = self.generate_hints_from_movelist(mov_list)
        n_mov_list = self.generate_all_moves_from_tips(mov_list, hint_list)
        moves_list = new_glob + n_mov_list
        return moves_list

    def GEN_expand_with_global_help(self, happened):
        """Assumes the board has been already expanded at least once"""
        c = False
        if not len(happened):
            print("wtf")
            return self.expand()
        fillmap = [[0 for _ in range(self.board_x)] for _ in range(self.board_y)]
        mov_list = []
        new_glob = self.global_help
        # all empty positions
        for new_piece in happened:
            for neigh in self.NEIGHBOURS:
                pos = new_piece[:2]
                v = new_piece[:2]
                y, x = tuple(map(add, v, neigh))
                if y >= 0 and y < self.board_y and x >= 0 and x < self.board_x and self.board[y][x][0] == "n":
                    if not fillmap[y][x] and (y, x) not in self.global_help:
                        fillmap[y][x] = 1
                        hint = self.generate_hints_1move((y, x))
                        for m in self.get_moves_for_1_hint(hint):
                            c = True
                            yield (y, x, m)
        return c


    def adjust_weights(self, moves):
        if moves < self.EARLY_GAME:
            self.weights_m = [2, 0.01, 0.01, 1, 1, 0.5, 0.1]
            self.weights_m = [2, 0.01, 0.01, 1, 1, -0.5, -0.1]
        elif moves < self.MIDGAME:
            self.weights_m = [2, 0.001, 0.001, 1, 1, 0.005, 0.0001]
            self.weights_m = [2, 0.001, 0.001, 1, 1, -0.005, -0.0001]
        else:
            self.weights_m = [3, 0.001, 0.001, 0.5, 0.5, 0, 0.00000000001]
            self.weights_o = [3, 0.001, 0.001, 0.5, 0.5, 0, 0.00000000001]

    def move(self):
        """ return list of moves:
                    []  ... if the player cannot move
                    [ [r1,c1,piece1], [r2,c2,piece2] ... [rn,cn,piece2] ] -place tiles to positions (r1,c1) .. (rn,cn)
        """
        global b
        b = 0
        global a
        a = 0

        self.adjust_weights(self.moves)
        val, mov = self.alphabeta(self.DEPTH, float("-inf"), float("+inf"), True)
        self.total_score += val
        self.moves += 1

        return mov


def count(board):
    a = 0
    suma = len(board) * len(board[0])
    for line in board:
        for char in line:
            a = a + 1 if char == "none" else a
    return a


res = [0] * 12


def play_one_game(a, b, c, d, tid=0):
    global res
    import draw as Drawer
    dr = Drawer.Drawer()
    boardRows = 12
    boardCols = boardRows
    board = [["none"] * boardCols for _ in range(boardRows)]

    board[boardRows // 2][boardCols // 2] = ["lldd", "dlld", "ddll", "lddl", "dldl", "ldld"][random.randint(0, 5)]

    # board = [['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'ldld', 'ldld', 'lddl', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'lddl', 'dlld', 'lldd', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'dlld', 'ldld', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'ddll', 'ldld', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'dlld', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'dldl', 'ddll', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'],
    #          ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none']]

    p1 = Player(board, "player1", 'd', "a", a, b)
    p2 = Player(board, "player2", 'l', "b", c, d)
    pp1 = p1
    pp2 = p2

    # test game. We assume that both player play correctly. In Brute/Tournament case, more things will be checked
    # like types of variables, validity of moves, etc...

    idx = 0
    max_time = 0
    while True:

        # call player for his move

        a = time.time_ns()
        rmove = p1.move()
        b = time.time_ns()
        print("Time:", (b - a) / 1000000)
        max_time = max(max_time, (b - a) / 1000000)
        # print(rmove)

        # rmove is: [ [r1,c1,tile1], ... [rn,cn,tile] ]
        # write to board of both players
        for move in rmove:
            row, col, tile = move
            p1.board[row][col] = tile
            p2.board[row][col] = tile

        # make png with resulting board
        dr.draw(p1.board, "game/move-{:04d}.png".format(idx))

        idx += 1

        if len(rmove) == 0:
            print("End:", idx)
            if count(p1.board):
                print("Unfilled!")
            print("Max time: ", max_time)
            print("Total scores:")
            print("Dark:" if p1.myColor == "d" else "Light:", p1.total_score)
            print("Dark:" if p2.myColor == "d" else "Light:", p2.total_score)
            if p1.algorithmName == "a":
                res[tid] = abs(p1.total_score * p2.total_score) / abs(p1.total_score - p2.total_score)
                print(abs(p1.total_score * p2.total_score) / abs(p1.total_score - p2.total_score))

                return abs(p1.total_score * p2.total_score) / abs(p1.total_score - p2.total_score)
            elif p2.algorithmName == "a":
                res[tid] = abs(p1.total_score * p2.total_score) / abs(p2.total_score - p1.total_score)
                return abs(p1.total_score * p2.total_score) / abs(p1.total_score - p2.total_score)
            else:
                assert False

            break
        p1, p2 = p2, p1  # switch players


def BRUTEFORCE():
    global res
    # call you functions from this block:
    from multiprocessing import Pool

    max_sc = [float("-inf"), 2, 6]
    for a in range(0, 8):
        with Pool(12) as p:
            result = p.starmap(play_one_game, [(a, b, max_sc[1], max_sc[2]) for b in range(0, 8)])
        if max(result) > max_sc[0]:
            max_sc = [max(result), a, result.index(max(result))]
            print("max,", max(result))

    print(max_sc)


def main():
    print(play_one_game(2, 6, 2, 6))
    # BRUTEFORCE()


if __name__ == "__main__":
    # debug()
    main()
