#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

import sys


def get_data(fn):
    slovnik = []
    with open(fn, "r") as file:
        for line in file:
            slovnik.append(line.strip())
    return slovnik


def find_end_words(slovnik, ending):
    slovo_list = []
    min_slovo = ""
    for slovo in slovnik:
        if slovo.endswith(ending):
            slovo_list.append(slovo)
            if len(slovo) <= len(min_slovo) or not min_slovo:
                min_slovo = slovo
    return len(slovo_list), min_slovo


def main():
    sl = get_data(sys.argv[1])
    ending = sys.argv[2]
    l, mins = find_end_words(sl, ending)
    print(l)
    print(mins if mins else None)


main()
