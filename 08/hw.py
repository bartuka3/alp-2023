#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

def import_data(fname):
    b = []
    with open(fname, "r") as f:
        for line in f:
            b.append(line.strip().split())
    return b


def test():
    b = import_data("maze.txt")
    print(b[0][0])

    # vše, kde je nahoře d
    for i, line in enumerate(b):
        for j, char in enumerate(line):
            if b[i - 1][j][3] == char[1]:
                print(i, j)
