#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT
from enum import IntEnum
from sys import argv

NEIGHBOURS = (
    (0, 1),
    (1, 0),
    (-1, 0),
    (0, -1)
)


class Color(IntEnum):
    """Path colors"""
    DARK = -1
    LIGHT = 1


class BoardPiece:
    def __init__(self, string):
        self.string = string
        if string == "none":
            self.list = [0, 0, 0, 0]
        else:
            self.list = [int(Color.LIGHT) if x == "l" else int(Color.DARK) for x in string]

    def is_connected(self, neighbour, neighbour_dy, neighbour_dx, color):
        """Tells us whether or not we can join with the neighbour"""
        print(neighbour.list)
        print(self.list)
        print(neighbour_dy, neighbour_dx)
        print("-------------------")
        # TODO
        assert neighbour_dy or neighbour_dx
        if neighbour.string == "none":  # Cannot connect to none
            return False
        if neighbour_dy:
            if neighbour_dy == 1:  # 1 0 --> 3 1
                return self.list[3] == neighbour.list[1] and self.list[3] == int(color)
            else:  # -1 0 --> 1 3
                return self.list[1] == neighbour.list[3] and self.list[1] == int(color)
        else:  # if neighbour_dx:
            if neighbour_dx == 1:  # 0 1 --> 2 0
                return self.list[2] == neighbour.list[0] and self.list[2] == int(color)
            else:  # 0 -1 --> 0 2:
                return self.list[0] == neighbour.list[2] and self.list[0] == int(color)

    def __str__(self):
        return self.string


def coord_is_in_constraint(y, x, *args):
    for arg in args:
        if arg[0] in range(0, y) and arg[1] in range(0, x):
            return True
        else:
            return False


def import_data(fname):
    board = []
    with open(fname, "r") as file:
        for line in file:
            board.append([BoardPiece(s) for s in line.split()])
    return board


def pretty_print_08(board):
    for line in board:
        print(" ".join([str(x.list) for x in line]))


T = 0


def flood_fill_with_puzzle_pieces(y, x, board, fill_map, piece: BoardPiece, count=1, color=Color.LIGHT):
    global T
    rets = 0
    for neigh in NEIGHBOURS:
        ny, nx = y + neigh[0], x + neigh[1]
        if coord_is_in_constraint(len(board), len(board[0]), (ny, nx)):  # Not bogus coordinates
            if not fill_map[ny][nx]:  # Already walked
                print(y, x, ny, nx)
                if piece.is_connected(board[ny][nx], *neigh, color):  # We can walk to the neighbour
                    print("True")
                    rets += 1
                    T += 1
                    print(count)
                    fill_map[ny][nx] = count
                    flood_fill_with_puzzle_pieces(ny, nx, board, fill_map, board[ny][nx], count + 1, color)

    if not rets:  # We likely can't go anywhere
        return count


def locate_side_to_sides(board):
    # TODO
    return


def main():
    fn = "vstup_1"
    # fn = sys.argv[1]

    board = import_data(fn)
    pretty_print_08(board)
    fill_map = [[0 for _ in range(len(board[0]))] for _ in range(len(board))]
    y, x = 2, 0
    count = flood_fill_with_puzzle_pieces(y, x, board, fill_map, board[y][x], count=1, color=Color.DARK)
    print(fill_map)
    print(T)
    return 0

# def test_adjacent():
#     a = BoardPiece("lddl")
#     b = BoardPiece("lldd")
#     print(a.is_connected(b,1,0,Color.LIGHT))

if __name__ == "__main__":
    main()
    # test_adjacent()