#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT
from enum import IntEnum
from sys import argv, exit

NEIGHBOURS = (
    (0, 1),
    (1, 0),
    (-1, 0),
    (0, -1)
)


def print_and_exit(*args):
    print(*args)
    exit()


class Color(IntEnum):
    """Path colors"""
    DARK = -1
    LIGHT = 1


class BoardPiece:
    def __init__(self, string):
        self.string = string
        if string == "none":
            self.list = [0, 0, 0, 0]
        else:
            self.list = [int(Color.LIGHT) if x == "l" else int(Color.DARK) for x in string]

    def can_be_connected(self, neighbour, neighbour_dy, neighbour_dx, color):
        """Tells us whether or not we can join with the neighbour"""
        assert neighbour_dy or neighbour_dx
        if neighbour.string == "none":  # Cannot connect to none
            return False
        if neighbour_dy:
            if neighbour_dy == 1:  # 1 0 --> 3 1
                return self.list[3] == neighbour.list[1] and self.list[3] == int(color)
            else:  # -1 0 --> 1 3
                return self.list[1] == neighbour.list[3] and self.list[1] == int(color)
        else:  # if neighbour_dx:
            if neighbour_dx == 1:  # 0 1 --> 2 0
                return self.list[2] == neighbour.list[0] and self.list[2] == int(color)
            else:  # 0 -1 --> 0 2:
                return self.list[0] == neighbour.list[2] and self.list[0] == int(color)

    def __str__(self):
        return self.string


def coord_is_in_constraint(y, x, *args):
    for arg in args:
        if arg[0] in range(0, y) and arg[1] in range(0, x):
            return True
        else:
            return False


def import_data(fname):
    board = []
    with open(fname, "r") as file:
        for line in file:
            board.append([BoardPiece(s) for s in line.split()])
    return board


def pretty_print_08(board):
    return 1
    for line in board:
        print(" ".join([str(x.list) for x in line]))


T = 0


def flood_fill_with_puzzle_pieces(y, x, board, fill_map, piece: BoardPiece, count=1, color=Color.LIGHT):
    global T
    rets = 0
    for neigh in NEIGHBOURS:
        ny, nx = y + neigh[0], x + neigh[1]
        if coord_is_in_constraint(len(board), len(board[0]), (ny, nx)):  # Not bogus coordinates
            if not fill_map[ny][nx]:  # Already walked
                print(y, x, ny, nx)
                if piece.can_be_connected(board[ny][nx], *neigh, color):  # We can walk to the neighbour
                    # print("True")
                    rets += 1
                    T += 1
                    # print(count)
                    fill_map[ny][nx] = count
                    flood_fill_with_puzzle_pieces(ny, nx, board, fill_map, board[ny][nx], count=count + 1,
                                                  color=color)

    if not rets:  # We likely can't go anywhere
        return count


def cycle_finding_flood_fill(y, x, board, fill_map, piece: BoardPiece, loop_map, count=1, color=Color.LIGHT):
    """fill_map is written into.
    """
    global T
    rets = 0
    for neigh in NEIGHBOURS:
        ny, nx = y + neigh[0], x + neigh[1]
        if coord_is_in_constraint(len(board), len(board[0]), (ny, nx)):  # Not bogus coordinates
            if piece.can_be_connected(board[ny][nx], *neigh, color):  # Can we move there?
                # print(y, x, ny, nx)
                if fill_map[ny][nx]:  # We can't walk to the neighbour
                    if loop_map[ny][nx]:  # We have already visited
                        if abs(T - fill_map[ny][nx]) > 1:  # we arent talking about a neighbour
                            # print("dsfsdfsdfs",color)
                            # print(loop_map)
                            # print(T)
                            # print("aaaaa", color)
                            # we found a loop
                            return True, T
                else:
                    # print("True")
                    rets += 1
                    T += 1
                    # print(count)
                    fill_map[ny][nx] = T
                    loop_map[ny][nx] = T
                    return cycle_finding_flood_fill(ny, nx, board, fill_map, board[ny][nx], loop_map, count=count + 1,
                                                    color=color)
    if not rets:
        return False, T


def suma_matrix(matrix):
    sumaa = 0
    for y in matrix:
        for x in y:
            sumaa += bool(x)
    return sumaa



def gen_fill_map(board):
    return [[0 for _ in range(len(board[0]))] for _ in range(len(board))]


def find_cycles_of_color(board, color):
    global T
    fill_map = gen_fill_map(board)
    num_cycles, max_cycle = 0, 0
    for y in range(len(board)):
        for x in range(len(board[0])):
            if not fill_map[y][x]:
                loop_map = gen_fill_map(board)
                T = 0
                has_a_cycle, curr_cycle = cycle_finding_flood_fill(y, x, board, fill_map, board[y][x], loop_map,
                                                                   color=color)
                # print("-----")
                num_cycles += int(has_a_cycle)
                if has_a_cycle:
                    max_cycle = max(max_cycle, curr_cycle)
    return num_cycles, max_cycle


def main():
    fn = "vstup_1"
    # fn = sys.argv[1]

    board = import_data(fn)
    pretty_print_08(board)
    fill_map = gen_fill_map(board)
    y, x = 2, 0
    count = flood_fill_with_puzzle_pieces(y, x, board, fill_map, board[y][x], count=1, color=Color.DARK)
    # print(fill_map)
    # print(T)
    return 0


def main_cycles():
    # fn = "vstup_l"
    fn = argv[1]

    board = import_data(fn)

    pretty_print_08(board)
    wh_n, wh_m = find_cycles_of_color(board, Color.LIGHT)
    bl_n, bl_m = find_cycles_of_color(board, Color.DARK)
    print_and_exit(wh_n, bl_n, wh_m, bl_m)
    return 0

if __name__ == "__main__":
    main_cycles()
    # test_adjacent()
