import random


def recursive_sum(line):
    if line:
        return recursive_sum(line[1:]) + line[0]
    else:
        return 0


#
# print(recursive_sum([1,4,5,54,2,4,2,58,9]))
# print(sum([1,4,5,54,2,4,2,58,9]))


def permutation(string, perm):
    return "".join([string[x] for x in perm])


def inverse_permutation(perm):
    np = [0] * len(perm)
    for i, val in enumerate(perm):
        np[val] = i
    return np


a = [3, 2, 0, 1]


def perm_test():
    print(permutation("ahoj", a))
    print(inverse_permutation(a))
    print(permutation(permutation("ahoj", a), inverse_permutation(a)))


# perm_test()

def is_in_constraint(y, x, *args):
    for arg in args:
        if arg[0] in range(0, y) and arg[1] in range(0, x):
            return True
        else:
            return False


NEIGHBOURS = (
    (0, 1),
    (1, 0),
    (-1, 0),
    (0, -1)
)


def vw(v, w):
    # print(v,w)
    # print([x + y for x, y in (v, w)])
    return [x + y for x, y in zip(v, w)]


def flood_fill(point, board, fill_matrix=None, count=0):
    global NEIGHBOURS
    for neighbour in NEIGHBOURS:
        ny, nx = vw(point, neighbour)
        if is_in_constraint(len(board), len(board[0]), (ny, nx)) and not board[ny][nx] and not fill_matrix[ny][nx]:
            # print(ny,nx)
            # print("asddddddddddddddddddddd")
            fill_matrix[ny][nx] = 2
            count += 1
            flood_fill((ny, nx), board, fill_matrix, count)
    return fill_matrix


def floodfill(a, r, c):
    stack = []
    stack.append([r, c])
    while stack:
        active = stack.pop()
        for neigh in NEIGHBOURS:
            roffset, coffser = neigh


def print_matrix(m):
    for r in m:
        for col in r:
            print(col, end=" ")
        print("")


def test_flood_fill():
    board = [[random.randint(0, 1) for _ in range(10)] for _ in range(10)]
    print_matrix(board)

    # board = [[0, 1, 0, 0, 0],
    #          [0, 1, 0, 0, 0],
    #          [1, 0, 0, 0, 0],
    #          [0, 1, 1, 1, 1]]
    fill_matrix = [[0 for _ in board[0]] for _ in board]
    y, x = random.randint(0, 5), random.randint(0, 5)
    print("--- --- --- ---")
    print(y, x)
    print("--- --- --- ---")

    print_matrix(flood_fill((0, 0), board, fill_matrix, 0))


test_flood_fill()
