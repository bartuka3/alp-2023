Y = 5
# O(y) = log_2 y

def hadej(y,L,R,func,change):
    # výsup, L, R, funkce, počet stávajících cyklů
    S = (L + R) / 2
    if cykly:
        if not func(S): return (L,R,S)
        if (func(R)>0 and func(S)>0) or (func(R)<0 and func(S)<0): # Posunujeme doprava
            return hadej(y,L,S,func,cykly-1)
        elif (func(L)>0 and func(S)>0) or (func(L)<0 and func(S)<0):
            return hadej(y,S,R,func,cykly-1)
        else:
            print(y,L,R)
            raise Exception("Unknown state")
    else:
        return (L,R,S)

;
a = hadej(Y,0,Y,lambda x: 2**x-Y,6)
print(a[2])
