Y = float(input())

def func(x):
    # Inverzní funkce to, co hledáme
    return 2**x-Y

def main():
    if Y>1:
        L = 0
        R = Y
    elif Y<1:
        L = -1/Y
        R = 0
    elif Y==0: return 0


    change = Y

    while abs(change) > 0.000000001:
        S = (L + R) / 2
        if (func(R) > 0 and func(S) > 0) or (func(R) < 0 and func(S) < 0):
            R = S
        elif (func(L) > 0 and func(S) > 0) or (func(L) < 0 and func(S) < 0):
            L = S
        change = S - (L + R) / 2
        # print(S,change)
    S = (L + R) / 2
    return S

print(main())