#!/bin/python3

from sys import exit


def raise_error():
    print("ERROR")
    exit()

class Cislo:
    def __init__(self,zaklad,number_str):
        self.zaklad = zaklad
        self.num_str = number_str
        self.convert_number_string()
        # print("INIT")
        # print(self.numL)
        # print(self.numR)

    def _convert_char(self,char: str) -> int:
        a = ord(char)
        if a in range(ord("a"), ord("z") + 1):
            # print("pismeno")
            return a - ord("a") + 10
        elif a in range(ord("0"), ord("9") + 1):
            # print("cislo")
            # print(int(char))
            return int(char)

        elif (a == ord(".")):
            # print("tecka")
            return ord(".")
        else:
            # Invalid character, exiting
            raise_error()
    def _convert_int(self,num:int) -> str:
        if num in range(0,10):
            return str(num)
        elif num in range(10,36+1):
            return str(chr(ord("a")+num-10))
        else:
            # print(num)
            raise_error()


    def convert_number_string(self):
        self.numC = [self._convert_char(o) for o in self.num_str]
        # print(self.numC)
        if self.numC.count(ord("."))>1: # Více desetinných teček
            # print("moc tecek")
            raise_error()
        for n in self.numC:
            if n>=self.zaklad and n!=ord("."):
                # Překročili jsme základ
                # print("prekroceni zakladu")
                # print(self.numC)
                raise_error()

        dot_ind = self.numC.index(ord("."))
        self.numL = self.numC[:dot_ind]
        self.numR = self.numC[dot_ind+1:]
        self.numC.remove(ord("."))
        return

    def dorovnej_nuly(cls, other):
        # Zaznamenání délek listů
        len_l1 = len(cls.numL)
        len_r1 = len(cls.numR)
        len_l2 = len(other.numL)
        len_r2 = len(other.numR)

        # print("<<<------")
        # print(cls.numL)
        # print(other.numL)
        # Přidání nul
        if len_l1 > len_l2:
            other.numL = [0] * (len_l1 - len_l2) + other.numL
        elif len_l2 > len_l1:
            cls.numL = [0] * (len_l2 - len_l1) + cls.numL
        else:
            other.numL = [0] + other.numL
            cls.numL = [0] + cls.numL
        if len_r1 > len_r2:
            other.numR = other.numR + [0] * (len_r1 - len_r2)
        elif len_r2 > len_r1:
            cls.numR = cls.numR + [0] * (len_r2 - len_r1)
        else:
            other.numR = other.numR + [0]
            cls.numR = cls.numR + [0]
        # print("---->>>>>")
        # print(cls.numL)
        # print(other.numL)
        # Assertace smysluplnosti
        assert len(cls.numL) == len(other.numL)
        assert len(cls.numR) == len(other.numR)
        assert cls.zaklad == other.zaklad

    def __mul__(cls, other):
        out = Cislo(cls.zaklad, cls.num_str)
        # Dorovnání nul, aby obě čísla měly stejný počet, popřípadě o jedno více
        # print("--------")
        # print(cls.numL)
        # print(other.numL)
        carka = len(cls.numR)+len(other.numR)
        # print(carka)
        # cls.dorovnej_nuly(other)

        # Tvorba bufferu výstupu

        out.numC = [0] * (2 * (max(len(cls.numC),len(other.numC))) + 2)
        # print("cls.numL")
        # print(cls.numL)


        zaklad = cls.zaklad
        # print(zaklad)


        for j in range(-1,-len(other.numC)-1,-1):
            # For each digit in the multiplying number
            for k in range(-1,-len(cls.numC)-1,-1):
                # For each digit in the multiplied number

                # print(j,k,len(other.numC),len(cls.numC))
                # print(cls.numC[k],other.numC[j],(cls.numC[k]*other.numC[j]))
                out.aaaaaaa = j+k,j+k-1
                out.numC[j+k-1] = out.numC[j+k-1] + (cls.numC[k]*other.numC[j])//zaklad
                out.numC[j+k] = out.numC[j+k] + (cls.numC[k]*other.numC[j])%zaklad


        # print("".join([ str(x) for x in out.numC[:-carka]+["."]+out.numC[-carka:] ]))

        # Sploštění výsledku do base(zaklad)
        while max(out.numC) >= zaklad:
            for l in range(len(out.numC)-1,0,-1):

                out.numC[l-1] = out.numC[l-1] + out.numC[l] // zaklad # Převod přes
                out.numC[l] = out.numC[l] % zaklad


        # Odebrání zbytečných nul
        numCstr = "".join([cls._convert_int(x) for x in out.numC[:-1]])
        # print(numCstr)
        lStr = "".join(numCstr[:-carka]).lstrip("0") if "".join(numCstr[:-carka]).lstrip("0") else "0"
        rStr = "".join(numCstr[-carka:]).rstrip("0") if "".join(numCstr[-carka:]).rstrip("0") else "0"


        return "".join(lStr+"."+rStr)



zaklad = int(input())
cislo_1 = Cislo(zaklad, input())
cislo_2 = Cislo(zaklad, input())





# PRIKLADY - DEBUG


# zaklad = int(6)
# cislo_1 = Cislo(zaklad, "504.05313")
# cislo_2 = Cislo(zaklad, "44.022")


# zaklad = int(2)
# cislo_1 = Cislo(zaklad,"1.1011")
# cislo_2 = Cislo(zaklad,"1.01")



# print("REAL")
# print(cislo_1.numL)
# print(cislo_2.numL)
print(cislo_1 * cislo_2)

