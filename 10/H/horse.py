#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT
from copy import deepcopy as dc
import sys

HORSE_MOVEMENT = ((2, 1), (2, -1),
                  (1, 2), (1, -2),
                  (-1, 2), (-1, -2),
                  (-2, 1), (-2, -1))


def load_board(fn):
    board = []
    with open(fn, "r") as file:
        for line in file:
            board.append(list(map(int, line.split())))
    return board


def n_plus_m(n, m, a=1, b=1):
    return tuple(a * x + b * y for x, y in zip(n, m))


def is_constrained_in_board(y, x, *args):
    for arg in args:
        # print(y,x,arg)
        if arg[0] in range(0, y) and arg[1] in range(0, x):
            return True
        else:
            return False


class Coordinates:
    def __init__(self, y, x):
        self.y = y
        self.x = x

    def __str__(self):
        return str(self.x) + " " + str(self.y)


class HorseState:

    def __init__(self, board, curr_position: tuple, previous_state=None):
        self.board = board
        self.current = curr_position
        self.prev = previous_state

    def state(self):
        return self.current

    def __eq__(self, other):
        return self.state() == other.state()

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return " ".join(list(map(str, self.state())))

    def __str__(self):
        return ", ".join(map(str, self.state() + n_plus_m(self.state(), self.prev.state(), b=-1)))

    def expansion(self):
        global HORSE_MOVEMENT
        res = []
        board_y, board_x = len(self.board), len(self.board[0])
        for neigh in HORSE_MOVEMENT:
            nyx = n_plus_m(self.current, neigh)

            if is_constrained_in_board(board_y, board_x, nyx) and int(self.board[nyx[0]][nyx[1]])!=1:
                res.append(HorseState(self.board, nyx, self))
        return res


def solve_bfs(start, finish_search_func=lambda x, y: x[-1], board=None):
    """Performs BFS
       Assumes that `start` has an expansion() function and
       that `start` is hashable.
    """
    q = [start]
    visited = {}
    walked = {}
    while len(q):
        curr = q.pop(0)
        nn = curr.expansion()
        for act in nn:
            # if act.current == (2, 2): print(act.current, nn)
            if str(act) not in visited:
                visited[str(act)] = 1
                q.append(act)
            if finish_search_func(act.board, act.current):
                return HorseState(board,act.current,act)
    # pretty_print_with_walked(board,visited)


def find_horse(board, val):
    for y, line in enumerate(board):
        for x, char in enumerate(line):
            if str(char) == str(val):
                # print(y, x)
                return y, x


def is_finish(board, position):
    return str(board[position[0]][position[1]]) == "4"


def backtrack(node: HorseState):
    prev = node.prev
    stack = []
    while prev:
        stack.append(prev)
        prev = prev.prev
    return reversed(stack)

def pretty_print_with_walked(board,walked):
    print(list(map(lambda x: [int(y) for y in x.split(", ")[:3]],walked)))
    for y, line in enumerate(board):
        for x, char in enumerate(line):
            print("\033[96m"+str(char)+"\033[0m" if [y,x] in map(lambda x: [int(y) for y in x.split(", ")[:2]],
                                                               walked) else
                  char,end=" ")
        print()
def main():
    # fn = "horse4.txt"
    fn = sys.argv[1]
    board = load_board(fn)
    path = solve_bfs(HorseState(board, find_horse(board, 2), None), is_finish, board)
    if not path:
        print("NEEXISTUJE")
    else:
        print(" ".join([repr(x) for x in list((backtrack(path)))[1:]]))

    pass


if __name__ == "__main__":
    main()
