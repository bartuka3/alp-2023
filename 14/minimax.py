#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT
"""
# Zero-sum games

## Max player - maximalises score
## Min player - minmalises score

hráč - l: min i max počítá skóre l

konec hry - ex. finitní skóre
jinak - skóre 0

"""

"""
Zrychlení
 
 
       ^  
   2      x
  /  \    / \
 1  (2)  1  (10)
  
  
alpha-beta pruning  
"""


def minimax(player, depth, isMax, a, b):
    # states = [player,depth,isMax]
    # while len(states):
    # b,d,m = states.pop()
    if not depth:
        return player.get_score(), [] #!!
        # skóre 0 --> heurestika
    tiles = player.expand()  # tiles: list(list([r,c,tile],[r,c,tile]),...)
    move_action = []
    move_value = None
    for act in tiles:  # ALL possible moves
        for cell in act:  # predictions
            r, c, tile = cell
            player.board[r][c] = tile

        v, _, a, b = minimax(player, depth - 1, not isMax, a, b)

        for cell in act:  # reversal of preds
            r, c, tile = cell
            player.board[r][c] = "none"
        if isMax:  # maximalising player
            if move_value is None or v > move_value:
                move_value = v
                move_action = act
        else:  # minimalising player
            if move_value is None or v < move_value:
                move_value = v
                move_action = act
    return move_value, move_action


class Player:
    def __init__(self, heuristics):
        self.board = []
        self.w1 = 1
        self.w2 = 1.5
        self.HEURISTICS = heuristics

    def move(self):
        a, b = 1, 1
        value, action = minimax(self, 3, True, a, b)
        return action

    def expand(self):
        pass

    def get_score(self):
        cntS = 0
        cntC = 0
        if self.HEURISTICS:
            return self.w1 * cntC + self.w2*cntS
        else:
            return cntC+cntS
        pass

    def get_heuristic(self):
        pass


def main():
    p1 = Player(False)
    p2 = Player(True)
    # who won got better values
    # manual approximation