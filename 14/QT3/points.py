#  Copyright (c) 2023.
#  bartuka3, FEL ČVUT
import math
from copy import deepcopy


# def input():
#     return "-1.5 2 0 2 1 2 2 2 3 2 4 2 -1.5 3 0 3 1 3 2 3"


def get_data():
    iline = list(map(float, input().split()))
    coords = []
    for i in range(len(iline) // 2):
        coords.append([iline[i * 2], iline[i * 2 + 1]])
    return coords


def get_avg(data):
    running_avg = [0, 0]
    count = 0
    for cord in data:
        running_avg[0] = running_avg[0] + cord[0]
        running_avg[1] = running_avg[1] + cord[1]
        count += 1
    return (running_avg[0] / count, running_avg[1] / count)


def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)


def get_closest_to_avg(data):
    avg = get_avg(data)
    min = [float("inf"), []]
    for coord in data:
        a = distance(coord, avg)
        if a < min[0]:
            min = [a, coord]
    return min


def brute_find_circle(data):
    for i, coord in enumerate(data):
        n_inside = 0
        inside = []
        diameter = distance(coord, (0, 0))
        for j, c in enumerate(data):
            a = distance(c, (0, 0))
            if a <= diameter:
                n_inside += 1
                inside.append(c)
        if n_inside == len(data) // 2:
            return coord


def main():
    data = get_data()
    first = data.index(get_closest_to_avg(data)[1])
    nd = deepcopy(data)
    data.sort()
    second = nd.index(brute_find_circle(data))

    print(first, second)


if __name__ == "__main__":
    main()
