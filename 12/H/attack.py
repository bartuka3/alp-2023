#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

from sys import argv
from math import ceil, sqrt

DEBUG = False

SLOVA = [1415933556, 1701737583, 1853124384, 544240229, 1886545252, 1919247469, 1701080421, 1684890996, 1835365408,
         544240239, 1886547827, 1919906676, 1869837413, 1937007904, 544105834, 1852140140, 1701473381, 1785488752,
         1818587251, 1701868393, 1886611744, 543647086, 1734700649, 1701734753, 1852399980, 1767992430, 1634496105,
         1819175200]


def euclid_nsd(a, b):
    """From KSP MFF"""
    a, b = (a, b) if b < a else (b, a)
    Aa, Ab = (1, 0) if b < a else (0, 1)
    Ba, Bb = (0, 1) if b < a else (1, 0)
    while b:
        Aa = Aa - (a // b) * Ab
        Ba = Ba - (a // b) * Bb
        a %= b
        a, b = b, a
        Aa, Ab = Ab, Aa
        Ba, Bb = Bb, Ba
    return a, 0, Ba


def fast_exp(a, k, n):
    """From KSP MFF"""
    if k == 0:
        return 1
    if k == 1.0:
        return a
    if k % 2 == 0:
        tmp = fast_exp(a, k / 2, n)
        # print(k)
        return (tmp * tmp) % n
    else:
        b = (a * fast_exp(a, k - 1, n)) % n
        # print(b)
        return b


def encode_string(a: str):
    """Morph a string into the requested encoding"""
    x = [0] * 4
    for i in range(len(a)):
        x[i] = ord(a[i])

    return ((x[0] * 256 + x[1]) * 256 + x[2]) * 256 + x[3]


def decode_string(a: int):
    x = [""] * 4
    mask = 0b11111111
    for i in range(4):
        x[i] = chr((a & (mask << (i * 8))) >> i * 8) if (a & (mask << (i * 8))) >> i * 8 else ""
    return "".join(reversed(x))


def load_numbers():
    if DEBUG:
        a = "727632176438 36244668976 814914613242 835595208469 315384816693 483612417694 400515700122 599076310518 552651231198 183702874860"
    else:
        a = input()
    return [int(x) for x in a.split()]


# def guess_non_prime(n, tries=10):
#     """for 10 tries it has a 1/2^10 accuracy"""
#     random.seed(n)
#     is_prime = True
#     for i in range(tries):
#         random_number = random.randint(2, n - 1)
#         is_prime = is_prime and (fast_exp(random_number, n - 1, n) == 1)
#     return is_prime


def find_dec_key(e, psi_n, Ba):
    """Assumes that Ba is fed from extended Euclid"""
    inv = Ba % psi_n
    assert (inv * e) % psi_n == 1
    return inv


def encrypt(slovo, e, n):
    return fast_exp(slovo, e, n) % n


def verify_key(n, h_e, input_words):
    for i, slovo in enumerate(SLOVA):
        e = encrypt(slovo, h_e, n)
        if e in input_words:  # Encrypt
            return True
    return False


def factorise(n):
    """Fermatova Faktorizace"""
    a = ceil(sqrt(n))
    b = a * a - n
    while sqrt(b) % 1 != 0:
        a += 1
        b = a ** 2 - n
    if b < 0:
        raise Exception("Není složené z 2 čísel")
    p = int(a - sqrt(b))
    q = int(a + sqrt(b))
    return p, q, (p - 1) * (q - 1)


def new_guess(n, input_words):
    p, q, psi_n = factorise(n)
    acc = 0
    for guessed_e in range(2 ** 17, 2 ** 20 + 1):  # MAGIC value
        acc += 1
        if DEBUG:
            if not acc % 10000:
                print(acc)
        nsd, _, Ba = euclid_nsd(guessed_e, psi_n)  # NSD
        if nsd == 1 and verify_key(n, guessed_e, input_words): # verifying we actually got the right key
            # likely good, getting decompose key
            d = find_dec_key(guessed_e, psi_n, Ba)
            return d


def main():
    if DEBUG:
        n = 952371739433
    else:
        n = int(argv[1])
    input_list = load_numbers()
    correct_key = new_guess(n, input_list)
    if correct_key:
        for word in input_list:
            print(decode_string(encrypt(word, correct_key, n)), end="")
    else:
        return
    pass


if __name__ == "__main__":
    main()
