Tento domácí úkol bude ohledně šifrování algoritmem RSA. Algoritmus RSA rozlišuje veřejný klíč a soukromý klíč. Pro oba klíče je potřeba mít dvě prvočísla p, q, která budou v našem případě “malá”, p<220 a q<220

.

Základem každého klíče je číslo n=p⋅q
. Druhé významné číslo pro definici klíčů je hodnota funkce φ(n)=(p−1)(q−1). Pro skutečně velká n (čísla o velikosti 22048) je pro výpočet funkce φ

nutné najít prvočísla p,q, což je v současné době prakticky neřešitelné.

Veřejný klíč je určen dvěma čísly (n,e)
, kdy pro e platí nsd(e,φ(n))=1 (funkce nsd() značí největšího společného dělitele), tedy čísla e a φ(n)

jsou nesoudělná.

Pokud již máme definovaný veřejný klíč, pak soukromý klíč je definován dvěma čísly (n,d)
, kde d=e−1∈Zφ(n). Číslo d lze spočítat pomocí rozšířeného Euklidova algoritmu. Rozšířený Euklidův algoritmus při hledání největšího společného dělitele najde i dvě čísla α a β, pro která platí nsd(e,φ(n))=α⋅e+β⋅φ(n). I přes to, že čísla α nebo β mohou být záporná, lze je použít k výpočtu d z čísel e a φ(n)

.

Pokud chce Bob poslat zprávu Alici, tak zprávu zakóduje veřejným klíčem Alice, tedy pomocí dvou čísel (n,e)
, která Alice zveřejnila. Pokud tedy Bob chce zakódovat číslo a, pak s využitím veřejného klíče Alice (n,e) spočte číslo b=ae∈Zn. Dešifrování privátním klíčem (n,d) se provede obdobně, pro kód b se spočte číslo c=bd∈Zn

. Číslo c je rovno původnímu číslu a.

Tedy pro zakódování čísla a = 1634234218 veřejným klíčem (n=1015282856477,e=914394312011)
je potřeba spočítat (1634234218914394312011)%1015282856477

. K výpočtu této mocniny je nutné použít algoritmus rychlého mocnění.

Algoritmus pro rozšířený Euklidův algoritmus i algoritmus rychlého mocnění můžete nastudovat třeba v této kuchařce celých čísel. 