#  Copyright (c) 2022.
#  bartuka3, FEL ČVUT

from sys import argv


def euclid_nsd(a, b):
    a, b = (a, b) if b < a else (b, a)
    Aa, Ab = (1, 0) if b < a else (0, 1)
    Ba, Bb = (0, 1) if b < a else (1, 0)
    while b:
        Aa = Aa - (a // b) * Ab
        Ba = Ba - (a // b) * Bb
        a %= b
        a, b = b, a
        Aa, Ab = Ab, Aa
        Ba, Bb = Bb, Ba
    return a, Aa, Ba


def fast_exp(a, k, n):
    if k == 0:
        return 1
    if k == 1.0:
        return a
    if k % 2 == 0:
        tmp = fast_exp(a , k / 2,n)
        # print(k)
        return (tmp * tmp) % n
    else:
        b = (a * fast_exp(a, k - 1,n)) % n
        # print(b)
        return b

def fast_exp_stack(a,k,n):
    st = [(a,k,r)]
    res = 0
    while st:
        a, k, r = st.pop()
        if k == 0:
            pass


def encode_string(a: str):
    """Morph a string into the requested encoding"""
    x = [0] * 4
    for i in range(len(a)):
        x[i] = ord(a[i])

    return ((x[0] * 256 + x[1]) * 256 + x[2]) * 256 + x[3]


def load_stdin():
    """Cut into chunks of 4 chars"""
    whole = input()
    str_seq = []
    for i in range(len(whole) // 4 + 1):
        cut_string = whole[4 * i:4 * min(i + 1, len(whole) - 1)]
        if cut_string:
            str_seq.append(cut_string)

    return str_seq


def encrypt():
    # n = int(argv[1])
    # e = int(argv[2])
    n = 854757130933
    e = 339289

    for string in load_stdin():
        print(fast_exp(encode_string(string), e, n) % n, end=" ")
    pass


if __name__ == "__main__":
    encrypt()
